# asusctl HwMon

_From me, most likely this is the final version on this project, but I
will use some of the developments from this project in my next project
[BAS HWMon](https://gitlab.com/165962/bhm)._

[Python](https://www.python.org/) versions v3.8, v3.9.

cli required: `sh`, `ps`, `cat`, `awk`, `echo`, `grep`, `nproc`.

cli optional: `iw`, `nmcli`, `amixer`, `pkexec`, `nvidia-smi`.

_Supports working with [asusctl](https://gitlab.com/asus-linux/asusctl)
versions v3.6.2, v3.7.0, v3.7.1, v3.7.2._

---

### Installing

[Debian](https://www.debian.org/) / [Ubuntu](https://ubuntu.com/):

```
sudo apt install python3-gi python3-gi-cairo gir1.2-gtk-3.0
```

[Fedora](https://getfedora.org/):

```
sudo dnf install python3-gobject gtk3
```

Unpack the archive `~/asusctl-hwmon`, go to the directory with `main.py`

Run:
```shell
~/asusctl-hwmon/main.py
```
or
```shell
python3 ~/asusctl-hwmon/main.py 
```
or create `~/.local/share/applications/asusctl-HwMon.desktop`:
```text
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=asusctl HwMon
Comment=asusctl HwMon
Exec=/home/user/asusctl-hwmon/main.py
Icon=/home/user/asusctl-hwmon/data/icons/life-line.svg
Terminal=false
Type=Application
```

---

### Uninstalling

Remove a desktop file:
```shell
rm ~/.local/share/applications/asusctl-HwMon.desktop
```
Remove the app:
```shell
rm -rf ~/asusctl-hwmon/
```
Remove the configuration directory:
```shell
rm -rf ~/.config/BAS.app.asusctl-HWMon
```

_You can also delete the [python](https://www.python.org/) packages you
don't need using the regular tools of your OS.
But be careful, for example, in [Fedora](https://getfedora.org/) 34
[Gnome](https://www.gnome.org/), all the
[python](https://www.python.org/) packages I need were already
pre-installed, which means that deleting them can have a bad effect on
the system._

---

### Screenshots

#### Debian 11 Gnome
![Debian 11 Gnome](./data/screenshots/debian-11-gnome.png)

#### Fedora 34 KDE
![Fedora 34 KDE](./data/screenshots/fedora-34-kde.png)

---

This app uses:
* [asus-linux](https://gitlab.com/asus-linux)
  * [asusctl](https://gitlab.com/asus-linux/asusctl)

---

### Trademarks

ASUS and ROG Trademark is either a US registered trademark or trademark
of ASUSTeK Computer Inc. in the United States and/or other countries.

Reference to any ASUS products, services, processes, or other information
and/or use of ASUS Trademarks does not constitute or imply endorsement,
sponsorship, or recommendation thereof by ASUS.

The use of ROG and ASUS trademarks within this website and associated
tools and libraries is only to provide a recognisable identifier to users
to enable them to associate that these tools will work with ASUS ROG
laptops.

---