#!/usr/bin/env python3

# encoding: utf-8
#
# pip install vext
# pip install vext.gi
#
# PyCharm
# Press the green button in the gutter to run the script.
# Press Shift+F10 to execute it or replace it with your code. Press
# Double Shift to search everywhere for classes, files, tool windows,
# actions, and settings. See PyCharm help at
# https://www.jetbrains.com/help/pycharm/

if __name__ == '__main__':
    import ctypes
    import os
    import sys
    import gi

    if gi:
        gi.require_version("Gtk", "3.0")
        gi.require_version("Notify", "0.7")

    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    from ctypes.util import find_library
    from src.Application import Application


    def set_proc_name(name):
        name = name.encode('ascii')
        libc.prctl(PR_SET_NAME, ctypes.c_char_p(name), 0, 0, 0)


    def get_proc_name():
        name = ctypes.create_string_buffer(16)
        libc.prctl(PR_GET_NAME, name, 0, 0, 0)
        return name.value

    libc = ctypes.CDLL(find_library('c'))

    PR_SET_NAME = 15
    PR_GET_NAME = 16

    application_name = "asusctl HWMon"

    set_proc_name(application_name)

    app = Application(application_name=application_name)
    app.run(sys.argv)
