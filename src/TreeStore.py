import json

from gi.repository import Gtk, GObject, Pango

from src.Emitter import Emitter


def read(path):
    try:
        output = open(path, "r").read().strip()
    except Exception as e:
        output = ""

        # TODO
        # print("Exception", path, e)

    return output


def ck(key, a, default):
    if key in a:
        return a[key]
    else:
        return default


def value_by_role(value, role):
    unit = ""
    if value == "":
        value = "-"
    elif role == "in":
        unit = "V"
        value = int(value) / 1000
    elif role == "fan":
        unit = "RPM"
    elif role == "temp":
        unit = "°C"
        value = int(value) / 1000
    elif role == "freq":
        unit = "MHz"
        value = int(value) / 1000000
    elif role == "curr":
        unit = "A"
        value = int(value) / 1000000
    elif role == "power":
        unit = "W"
        value = int(value) / 1000000
    return [str(value), unit]


C_ID = 0
C_PID = 1
C_IND = 2
C_LEVEL = 3
C_LENGTH = 4
C_LABEL = 5
C_INPUT = 6
C_PATH = 7
C_ROLE = 8
C_PROP = 9
C_NUMBER = 10
C_TYPE = 11


class TreeStore(Gtk.TreeStore):
    def __init__(self, application: Gtk.Application):
        super().__init__()
        self._j = 0
        self._app = application
        self._count = 0
        self.columns = {}
        self._config = ()

        with open("./data/config/columns.json", encoding='utf-8') as f:
            self._config = json.load(f)

    def lang(self):
        for cnf in self._config:
            # c = self.columns[cnf["id"]]
            title = self._app.lang(cnf["column"]["title"])
            self.columns[cnf["id"]].set_title(title)

    @staticmethod
    def model_create_foreach_func(tree_store, tree_path, tree_iter):
        if tree_path:
            mon_role = tree_store.get_value(tree_iter, 8)
            if not mon_role == "" and not mon_role == "hwmon":
                value = read(tree_store.get_value(tree_iter, 9))
                tree_store.set_value(tree_iter, 1, value)
                value = value_by_role(value, mon_role)
                tree_store.set_value(tree_iter, 2, value[0])
                tree_store.set_value(tree_iter, 3, value[1])

    @staticmethod
    def model_update_foreach_func(tree_store, tree_path, tree_iter):
        if tree_path:
            mon_role = tree_store.get_value(tree_iter, 8)
            if not mon_role == "" and not mon_role == "hwmon":
                value = read(tree_store.get_value(tree_iter, 9))

                # TODO
                ind = tree_store.get_value(tree_iter, 10)
                if ind == "k10temp-tctl":
                    Emitter.emit("update-k10temp-tctl",
                                 value_by_role(value, mon_role))
                elif ind == "amdgpu-edge":
                    Emitter.emit("update-amdgpu-edge",
                                 value_by_role(value, mon_role))
                elif ind == "nvme-composite":
                    Emitter.emit("update-nvme-composite",
                                 value_by_role(value, mon_role))
                elif ind == "iwlwifi_1-temp":
                    Emitter.emit("update-iwlwifi_1-temp",
                                 value_by_role(value, mon_role))
                if mon_role == "fan":
                    Emitter.emit("update-fan", value)

                if not value == tree_store.get_value(tree_iter, 1):
                    tree_store.set_value(tree_iter, 1, value)
                    value = value_by_role(value, mon_role)
                    tree_store.set_value(tree_iter, 2, value[0])
                    tree_store.set_value(tree_iter, 3, value[1])

    def add_children(self, pid, tree=None, parent_iter=None):
        # if children is None:
        #     children = []
        children = list(filter(lambda x: x[C_PID] == pid, tree))
        # print(children)
        # exit(0)
        for row in children:
            child_iter = self.append(parent_iter)
            self.set(child_iter, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], [
                row[C_LABEL],
                row[C_INPUT],
                "",  # value
                "",  # child["unit"],
                row[C_ID],
                row[C_PROP],
                row[C_NUMBER],
                row[C_TYPE],
                row[C_ROLE],
                row[C_PATH],
                row[C_IND]
            ])
            if row[C_LENGTH] > 0:
                self.add_children(row[C_ID], tree, child_iter)

    @staticmethod
    def sort(tree):
        return sorted(tree, key=lambda x: (x[C_LEVEL], x[C_LABEL].lower()),
                      reverse=False)
        # children = sorted(children, key=lambda kv: kv["label"].lower())
        # for child in children:
        #     child["children"] = self.sort(child["children"])
        # return children

    def create(self, rows):
        column_types = []

        rows = self.sort(rows)

        for _0 in self._config:
            column_types.append(GObject.TYPE_STRING)

        self.set_column_types(column_types)
        self.add_children("", rows)
        self._treeView.set_model(self)
        self.foreach(self.model_create_foreach_func)

        normal = Gtk.CellRendererText(weight=Pango.Weight.NORMAL)
        normal1 = Gtk.CellRendererText(weight=Pango.Weight.NORMAL)
        normal1.set_property("xalign", Pango.Alignment.CENTER)

        self.columns = {}
        i = 0
        for item in self._config:
            cnf = item["column"]
            cell = normal
            # if "prop" in item["field"]:
            #     cell = normal1
            if "value" in item["field"]:
                cell = normal1
            # if "input" in item["field"]:
            #     cell = normal1
            # if "number" in item["field"]:
            #     cell = normal1

            title = self._app.lang(ck("title", cnf, ""))

            column = Gtk.TreeViewColumn()
            column.set_title(title)
            column.set_visible(ck("visible", cnf, True))
            column.set_spacing(ck("spacing", cnf, 0))
            column.set_clickable(True)
            column.set_resizable(ck("resizable", cnf, True))
            column.set_min_width(ck("min_width", cnf, 24))
            column.set_max_width(ck("max_width", cnf, -1))
            column.set_fixed_width(ck("fixed_width", cnf, -1))
            column.set_sort_indicator(ck("sort_indicator", cnf, False))

            header_button = column.get_button()
            header_button.connect("button-press-event",
                                  self.header_clicked)

            column.pack_start(cell, True)
            column.add_attribute(cell, "text", i)

            self._treeView.insert_column(column, i)

            self.columns[item["id"]] = column

            i = i + 1

    def update(self):
        self.foreach(self.model_update_foreach_func)

    def header_menu_clicked(self, cid):
        visible = not self.columns[cid].get_visible()
        self.columns[cid].set_visible(visible)

        # TODO save config columns
        self._config[cid]["column"]["visible"] = visible
        with open("./data/config/columns.json", "w",
                  encoding="utf-8") as f:
            json.dump(self._config, f, ensure_ascii=False, indent=4)

    def header_clicked(self, widget, event):
        if not isinstance(widget, Gtk.Button):
            raise TypeError("%r is not a gtk.Button" % widget)
        if event.button == 3:
            menu = Gtk.Menu()
            for cnf in self._config:
                c = self.columns[cnf["id"]]
                title = self._app.lang(cnf["column"]["title"])
                item = Gtk.CheckMenuItem(title)
                item.set_active(c.get_visible())
                item.connect_object("activate", self.header_menu_clicked,
                                    cnf["id"])
                menu.append(item)
            menu.show_all()
            Gtk.Menu.popup(menu, None, None, None, None, event.button,
                           event.time)
