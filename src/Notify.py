class Notify(object):
    @staticmethod
    def fan_mode(value: int) -> tuple:
        return ("The fan mode has been changed",
                "New fan mode is {0}.".format(value))

    @staticmethod
    def gfx_mode(value: int) -> tuple:
        return ("The graphics mode has been changed",
                "New graphics mode is {0}.".format(value))

    @staticmethod
    def chg_limit(value: int) -> tuple:
        return ("The battery charge limit has been changed",
                "New battery charge limit is {0}.".format(value))

    @staticmethod
    def kbd_bright(value: int) -> tuple:
        return ("The keyboard brightness has been changed",
                "New keyboard brightness is {0}.".format(value))

    @staticmethod
    def kbd_brightness(value: int) -> tuple:
        return ("The keyboard brightness has been changed",
                "New keyboard brightness is {0}.".format(value))

    @staticmethod
    def screen_brightness(value: int) -> tuple:
        return ("The screen brightness has been changed",
                "New screen brightness is {0}.".format(value))

    @staticmethod
    def wifi_powered(value: int) -> tuple:
        return ("The wi-fi powered has been changed",
                "New wi-fi powered is {0}.".format(value))

    @staticmethod
    def wifi_power_save(value: int) -> tuple:
        return ("The wi-fi power save has been changed",
                "New wi-fi power save is {0}.".format(value))

    @staticmethod
    def bluez_powered(value: int) -> tuple:
        return ("The bluetooth powered has been changed",
                "New bluetooth powered is {0}.".format(value))

    @staticmethod
    def sound_master_on(value: int) -> tuple:
        return ("The sound master has been changed",
                "New sound master is {0}.".format(value))

    @staticmethod
    def sound_master_volume(value: int) -> tuple:
        return ("The sound master volume has been changed",
                "New sound master volume is {0}%.".format(value))

    @staticmethod
    def sound_capture_on(value: int) -> tuple:
        return ("The sound capture has been changed",
                "New sound capture is {0}.".format(value))

    @staticmethod
    def sound_capture_volume(value: int) -> tuple:
        return ("The sound capture volume has been changed",
                "New sound capture volume is {0}%.".format(value))
