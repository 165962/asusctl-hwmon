# encoding: utf-8
#
# SettingsWindow
#

from gi.repository import Gtk

from src import Emitter
from src.SettingsTree import SettingsTree
from src.views import HBox, Check, Combo, Label, Interval


class SettingsWindow(Gtk.Window):
    def __init__(self, *args, **kwargs):
        super().__init__(name="SettingsWindow", *args, **kwargs)
        self.set_default_size(1400, 920)
        self._app = self.get_application()

        """ Interval """
        self._interval_label = Label()
        self._interval = Interval()
        self._interval.on_change_mode = self._app.set_interval_mode
        self._interval.on_change_delay = self._app.set_delay
        interval_box = HBox()
        interval_box.pack_start(self._interval_label, False, False, 0)
        interval_box.pack_start(self._interval.spin, False, False, 0)
        interval_box.pack_start(self._interval.combo, False, False, 0)

        """ Language """
        items = {}
        for key, value in enumerate(self._app.get_languages()):
            items[key] = value
        label = Label()
        combo = Combo(items=items, value=self._app.get_lang_id())
        combo.on_changed = self._app.set_lang
        language_box = HBox()
        language_box.pack_start(label, False, False, 0)
        language_box.pack_start(combo, False, False, 0)
        self._language_label = label

        """ Hide app name """
        label = Label()
        check = Check()
        check.set_value(self._app.config.hide_app_name)
        check.on_changed = self._app.set_hide_app_name
        show_app_name_box = HBox()
        show_app_name_box.pack_start(label, False, False, 0)
        show_app_name_box.pack_start(check, False, False, 0)
        self._show_app_name_label = label

        """ Table """
        self._table = SettingsTree(self._app.lang)

        self.lang()

        """ Content """
        scroll0 = Gtk.ScrolledWindow()
        scroll1 = Gtk.ScrolledWindow()
        vbox = Gtk.VBox()

        scroll1.add(self._table.get_widget())
        vbox.pack_start(language_box.widget, False, False, 0)
        vbox.pack_start(interval_box.widget, False, False, 0)
        vbox.pack_start(show_app_name_box.widget, False, False, 0)
        vbox.pack_start(scroll1, True, True, 16)
        scroll0.add(vbox)
        self.add(scroll0)

        vbox.set_margin_top(16)
        vbox.set_margin_start(32)
        vbox.set_margin_end(32)
        vbox.set_margin_bottom(16)

        """ Interval """
        self._interval.set_mode(self._app.config.interval_mode)
        self._interval.set_delay(self._app.config.delay)
        Emitter.on("change-interval-mode", self._interval.set_mode)
        Emitter.on("change-interval-delay", self._interval.set_delay)

        Emitter.on("change-lang", self.on_change_lang)
        Emitter.on("change-hide-app-name", self.on_change_hide_app_name)

        self.show_all()

    def _window_title(self, value):
        if value:
            f = "{win_name}"
        else:
            f = "{win_name} - {app_name}"
        title = f.format(
            app_name=self._app.application_name,
            win_name=self._app.lang("Settings")
        )
        self.set_title(title)

    def lang(self):
        self._window_title(self._app.config.hide_app_name)

        self._language_label.set_label(self._app.lang("Language: "))

        self._interval_label.set_label(self._app.lang("Interval: "))
        self._interval.set_items({0: self._app.lang("ms."),
                                  1: self._app.lang("sec.")})

        self._show_app_name_label.set_label(
            self._app.lang("Hide the app name in the title "))

    def on_change_lang(self):
        self.lang()
        self._table.lang()

    def on_change_hide_app_name(self, value):
        self._window_title(value)

    def on_destroy(self, _widget=None, *_data) -> None:
        self.hide()

    def close(self, _widget=None, *_data) -> None:
        self.hide()
        self.destroy()

    def destroy(self):
        Emitter.off("change-lang", self.on_change_lang)
        Emitter.off("change-hide-app-name", self.on_change_hide_app_name)
        Emitter.off("change-interval-mode", self._interval.set_mode)
        Emitter.off("change-interval-delay", self._interval.set_delay)
        self._interval.destroy()
