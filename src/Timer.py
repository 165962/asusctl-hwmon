# encoding: utf-8
#
# Timer
#

from gi.repository import GObject


class Timer:
    @staticmethod
    def set_timeout(milliseconds: int, handler: callable):
        return GObject.timeout_add(milliseconds, handler)

    @staticmethod
    def clear_timeout(timeout) -> None:
        GObject.source_remove(timeout)
