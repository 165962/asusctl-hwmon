# encoding: utf-8
#

from gi.repository import Gtk, GLib


class DialogRemoveProfile(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(self, transient_for=parent, flags=0)
        self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                         Gtk.STOCK_OK, Gtk.ResponseType.OK)
        title = "Remove a profile - " + GLib.get_application_name()
        self.set_title(title)
        # self.set_default_size(150, 100)
        msg = "You are sure that you want to remove the profile?"
        label = Gtk.Label(label=msg)
        box = self.get_content_area()
        box.set_margin_top(8)
        box.set_margin_start(8)
        box.set_margin_end(8)
        box.set_margin_bottom(8)
        label.set_margin_bottom(16)
        box.pack_start(label, False, False, 0)
        self.show_all()
