# encoding: utf-8
#

import math

from gi.repository import Gtk
from src.Emitter import Emitter


class DialogProfileFanCurve(Gtk.Dialog):
    def __init__(self, application: Gtk.Application, parent: Gtk.Window,
                 model):
        super().__init__(self, transient_for=parent, flags=0)
        self.add_buttons(Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE)

        self._app = application
        Emitter.on("change-lang", self.on_lang)

        self._head = [
            ["PAIR", "№", "CELSIUS", "PERCENTAGE OF ROTATION"],
            ["MIN", "MAX", "VALUE", "MIN", "MAX", "VALUE"]]

        self._step = 10
        self._line = "{celsius}c:{percent}%"
        self._store = []
        self._model = model
        self._title = None
        self._ignore = False
        self._config = [
            [30, 0, 100],
            [49, 0, 100],
            [50, 0, 100],
            [69, 0, 100],
            [70, 34, 100],
            [89, 51, 100],
            [90, 61, 100],
            [109, 61, 100]
        ]
        self._head_widgets = []

        self._title = Gtk.Label()
        self._apply = Gtk.Button()
        self._apply.connect("clicked", self._on_apply)
        self._create_table()

        box = self.get_content_area()
        box.pack_start(self._title, False, False, 0)
        box.pack_start(self._table, False, False, 0)
        box.pack_start(self._apply, False, False, 0)

        box.set_margin_top(16)
        box.set_margin_start(32)
        box.set_margin_end(32)
        box.set_margin_bottom(16)

        self._table.set_margin_top(16)
        self._apply.set_margin_top(16)
        self._apply.set_margin_bottom(16)

        self.lang()
        self.show_all()

    def _window_title(self, value):
        if value:
            f = "{win_name}"
        else:
            f = "{win_name} - {app_name}"
        title = f.format(
            app_name=self._app.application_name,
            win_name=self._app.lang("Profile fan curve")
        )
        self.set_title(title)

    def _read_data(self):
        data = self._model.get_profile_fan_curve()
        if data:
            temp = data
            data = []
            for line in temp.split(","):
                item = line[:-1].split("c:")
                data.append([int(item[0]), int(item[1])])
        return data

    def _check_celsius(self, index: int):
        j = math.floor(index / 2)
        i = j * 2
        get_widget = self._store[index][0]
        min_widget = self._store[i][0]
        max_widget = self._store[i + 1][0]
        min_value = self._config[i][0]
        max_value = self._config[i + 1][0]
        value = get_widget.get_value_as_int()
        value1 = min_widget.get_value_as_int()
        value2 = max_widget.get_value_as_int()
        if value1 < min_value:
            value1 = min_value
        if value1 > max_value - self._step:
            value1 = max_value - self._step
        if value2 > max_value:
            value2 = max_value
        if value2 < min_value + self._step:
            value2 = min_value + self._step
        if value < get_widget.old_value:
            if value2 < value1 + self._step:
                value1 = value2 - self._step
        elif value > get_widget.old_value:
            if value1 > value2 - self._step:
                value2 = value1 + self._step
        min_widget.set_value(value1)
        max_widget.set_value(value2)
        get_widget.old_value = get_widget.get_value_as_int()

    def _check_percent(self, index: int):
        widget = self._store[index][1]
        value = widget.get_value_as_int()
        v = value
        if value < widget.old_value:
            for i in range(index, -1, -1):
                w = self._store[i][1]
                min_value = self._config[i][1]
                # max_value = self._config[i][2]
                if v < min_value:
                    v = min_value
                    w.set_value(v)
                if v < w.get_value_as_int():
                    w.set_value(v)
        elif value > widget.old_value:
            for i in range(index + 1, 8):
                w = self._store[i][1]
                if v > w.get_value_as_int():
                    w.set_value(v)
        widget.old_value = widget.get_value_as_int()

    def _create_table(self):
        self._table = Gtk.Grid()
        self._table.set_row_spacing(0)
        self._table.set_column_spacing(0)
        self._table.set_column_homogeneous(False)
        self._table.get_style_context().add_class("table")
        self._create_table_head()
        self._create_table_body()

    def _create_table_head(self):
        self._head_widgets = []
        for i, data in enumerate(self._head):
            self._head_widgets.append([])
            for j, label in enumerate(data):
                widget = Gtk.Label(label=label)
                context = widget.get_style_context()
                context.add_class("item")
                if i == 0:
                    context.add_class("top")
                    if j == 0:
                        context.add_class("left")
                self._head_widgets[i].append(widget)

        self._table.attach(self._head_widgets[0][0], 0, -2, 1, 2)
        self._table.attach(self._head_widgets[0][1], 1, -2, 1, 2)
        self._table.attach(self._head_widgets[0][2], 2, -2, 3, 1)
        self._table.attach(self._head_widgets[0][3], 5, -2, 3, 1)

        self._table.attach(self._head_widgets[1][0], 2, -1, 1, 1)
        self._table.attach(self._head_widgets[1][1], 3, -1, 1, 1)
        self._table.attach(self._head_widgets[1][2], 4, -1, 1, 1)
        self._table.attach(self._head_widgets[1][3], 5, -1, 1, 1)
        self._table.attach(self._head_widgets[1][4], 6, -1, 1, 1)
        self._table.attach(self._head_widgets[1][5], 7, -1, 1, 1)

    def _create_table_body(self):
        format1 = "{num}."
        format2 = "{min}°C"
        format3 = "{max}°C"
        format4 = "{min}%"
        format5 = "{max}%"

        data = self._read_data()
        for i in range(0, 8):
            top = i
            num = i + 1
            even = num & 1
            pair = math.ceil(i / 2 + 1)
            start = -1
            widgets = []
            min_p = self._config[i][1]
            max_p = self._config[i][2]
            if even:
                min_c = self._config[i][0]
                max_c = self._config[i + 1][0] - self._step
            else:
                min_c = self._config[i - 1][0] + self._step
                max_c = self._config[i][0]
            if data:
                get_c = data[i][0]
                get_p = data[i][1]
            else:
                get_c = min_c
                get_p = min_p
            self._store.append((
                self._create_spin_button(i, get_c, min_c, max_c),
                self._create_spin_button(i, get_p, min_p, max_p)
            ))
            if even:
                widgets.append(Gtk.Label(label=format1.format(num=pair)))
            widgets.append(Gtk.Label(label=format1.format(num=num)))
            widgets.append(Gtk.Label(label=format2.format(min=min_c)))
            widgets.append(Gtk.Label(label=format3.format(max=max_c)))
            widgets.append(self._store[i][0])
            widgets.append(Gtk.Label(label=format4.format(min=min_p)))
            widgets.append(Gtk.Label(label=format5.format(max=max_p)))
            widgets.append(self._store[i][1])
            for w_id, widget in enumerate(widgets):
                context = widget.get_style_context()
                context.add_class("item")
                if even and w_id == 0:
                    context.add_class("left")
            if even:
                start = 0
                self._table.attach(widgets[start], 0, top, 1, 2)
            self._table.attach(widgets[start + 1], 1, top, 1, 1)
            self._table.attach(widgets[start + 2], 2, top, 1, 1)
            self._table.attach(widgets[start + 3], 3, top, 1, 1)
            self._table.attach(widgets[start + 4], 4, top, 1, 1)
            self._table.attach(widgets[start + 5], 5, top, 1, 1)
            self._table.attach(widgets[start + 6], 6, top, 1, 1)
            self._table.attach(widgets[start + 7], 7, top, 1, 1)

    def _create_spin_button(self, index: int, value: int, min_value: int,
                            max_value: int) -> Gtk.SpinButton:
        adjustment = Gtk.Adjustment(value, min_value, max_value, 1, 0, 0)
        widget = Gtk.SpinButton()
        widget.set_adjustment(adjustment)
        widget.set_alignment(Gtk.Justification.CENTER)
        # widget.set_size_request(128, -1)
        widget.set_range(min_value, max_value)
        widget.index = index
        widget.old_value = value
        # widget.connect("value-changed", self._on_changed)
        widget.connect("output", self._on_changed)
        return widget

    def _on_apply(self, _widget):
        data = []
        for i in range(0, 8):
            celsius = self._store[i][0].get_value_as_int()
            percent = self._store[i][1].get_value_as_int()
            line = self._line.format(celsius=celsius, percent=percent)
            data.append(line)
        self._model.set_profile_fan_curve(",".join(data))

    def _on_changed(self, widget: Gtk.SpinButton):
        if not self._ignore:
            self._ignore = True
            self._check_celsius(widget.index)
            self._check_percent(widget.index)
            self._ignore = False

    def lang(self):
        self._window_title(self._app.config.hide_app_name)

        self._title.set_label(self._app.lang("Profile fan curve"))
        self._apply.set_label(self._app.lang("Apply"))

        for i, data in enumerate(self._head):
            for j, label in enumerate(data):
                self._head_widgets[i][j].set_label(self._app.lang(label))

    def on_lang(self):
        print("--------------------------")
        self.lang()

    def destroy(self):
        Emitter.off("change-lang", self.on_lang)
        return super().destroy()
