# encoding: utf-8
#
# Keyboard
# Keyboard brightness: 0 - off,  1 - low,  2 - medium , 3 - high, parent
#

from src.models.BaseDbus import BaseDbus


class Kbd(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {"brightness": [None, []]}
        self._dependence = {
            "set_brightness": ["set_brightness"],
            "get_brightness": ["get_brightness", "set_brightness"],
            "min_brightness": ["get_brightness"],
            "max_brightness": ["get_brightness"],
            "list_brightness": ["get_brightness"]
        }
        self._service_name = "org.freedesktop.UPower"
        self._object_path = "/org/freedesktop/UPower/KbdBacklight"
        self._interface_name = "org.freedesktop.UPower.KbdBacklight"

    def init(self):
        self._create_dbus("system", (
            ("upower", self._service_name, self._object_path,
             self._interface_name, self._on_signal),))

        if self.is_method("get_brightness"):
            self._log("Init Kbd")
            self._log("    Brightness:", self.get_brightness())
            if self.is_method("max_brightness"):
                self._log("    MaxBrightness:", self.max_brightness())

    # Brightness

    def set_brightness(self, value: int) -> None:
        self._luck["set_brightness"](value)

    def get_brightness(self) -> int:
        return self._luck["get_brightness"]()

    @staticmethod
    def min_brightness() -> int:
        return 0

    def max_brightness(self) -> int:
        return self._luck["max_brightness"]()

    def list_brightness(self) -> dict:
        data = {}
        max_value = self.max_brightness()
        if max_value:
            for i in range(0, max_value + 1):
                data[i] = str(i)
        return data

    # Private

    def _set_brightness(self, value: int) -> None:
        self._dbus_proxy["upower"].SetBrightness("(i)", value)

    def _get_brightness(self) -> int:
        return self._dbus_proxy["upower"].GetBrightness()

    def _max_brightness(self) -> int:
        return self._dbus_proxy["upower"].GetMaxBrightness()

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        # BrightnessChangedWithSource
        if proxy and sender_name and signal_name == "BrightnessChanged":
            value = int(parameters[0])
            self._run_handlers("brightness", value)
