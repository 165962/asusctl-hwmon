# encoding: utf-8
#
# BaseDbus
#

import xml.etree.ElementTree as ElementTree

from gi.repository import Gio, GLib

from src.dbus_map import dbus_map
from src.models.BaseModel import BaseModel


def indent(elem, level=0):
    i = "\n" + level * "  "
    j = "\n" + (level - 1) * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for subelem in elem:
            indent(subelem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = j
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = j
    return elem


class BaseDbus(BaseModel):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._spec = {}
        self._luck = {}

        self._handlers = {}
        self._dependence = {}
        self._dbus_map = dbus_map
        self._dbus_proxy = {}
        self._dependence = {}
        self._service_name = ""
        self._object_path = ""

    def init(self):
        pass

    def is_method(self, name: str):
        if name in self._dependence:
            count = 0
            length = len(self._dependence[name])
            if length:
                for key in self._dependence[name]:
                    if key in self._luck:
                        count += 1
                if count == length:
                    return True
                else:
                    return False
        return False

    # Private

    def _introspect(self, interface, object_path):
        proxy = Gio.DBusProxy.new_sync(
            self._bus, Gio.DBusProxyFlags.NONE, None, interface,
            object_path, 'org.freedesktop.DBus.Introspectable')
        try:
            res = proxy.call_sync(
                method_name='Introspect',
                parameters=None,
                flags=Gio.DBusCallFlags.NONE,
                timeout_msec=-1,
                cancellable=None)
        except Exception as e:
            # self._log("Exception BaseDbus::_introspect", e)
            res = None
        if not res:
            return None
        return Gio.DBusNodeInfo.new_for_xml(res[0])

    def _introspect_interface(self, interface, object_path):
        rows = list(filter(lambda x:
                           x[0] == self._service_name and
                           x[1] == object_path and
                           x[2] == interface.name, self._dbus_map))
        ni = ElementTree.SubElement(self._xml_node, "interface",
                                    name=interface.name)
        for prop in interface.properties:
            for row in list(filter(lambda x:
                                   x[3] == prop.name and
                                   x[5] == prop.signature, rows)):
                if row[6] != "" and row[7] != "":
                    self._luck[row[6]] = getattr(self, row[7])
                ElementTree.SubElement(ni, "property", name=prop.name,
                                       type=prop.signature,
                                       access="read")
        for method in interface.methods:
            for row in list(filter(lambda x: x[4] == method.name, rows)):
                self._introspect_method(ni, row, method)
        for signal in interface.signals:
            ns = ElementTree.SubElement(ni, "signal", name=signal.name)
            for arg in signal.args:
                ElementTree.SubElement(ns, "arg", name=arg.name,
                                       type=arg.signature)

    def _introspect_method(self, xml_node, row, method):
        ni = xml_node
        nm = ElementTree.SubElement(ni, "method", name=method.name)
        signatures = ""
        for arg in method.in_args:
            signatures += arg.signature
            ElementTree.SubElement(nm, "arg", name=arg.name,
                                   type=arg.signature, direction="in")
        signatures += "|"
        for arg in method.out_args:
            signatures += arg.signature
            if method.name == 'ActiveData':
                ElementTree.SubElement(nm, "arg", type="",
                                       direction="out")
            else:
                ElementTree.SubElement(nm, "arg", type=arg.signature,
                                       direction="out")
        if row[5] == signatures:
            if row[6] != "" and row[7] != "":
                self._luck[row[6]] = getattr(self, row[7])

    def _create_dbus(self, bus_type: str, bus_proxy: tuple) -> None:
        object_paths = {}
        for item in bus_proxy:
            object_paths[item[2]] = item[2]
        self._xml_node = ElementTree.Element("node")
        for path in object_paths:
            if bus_type == "session":
                _bus_type = Gio.BusType.SESSION
            else:
                _bus_type = Gio.BusType.SYSTEM
            try:
                self._bus = Gio.bus_get_sync(_bus_type, None)
            except Exception as e:
                print("Exception BaseDebus::_create_dbus", e)
                return None
            dbus_node_info = self._introspect(self._service_name, path)
            if dbus_node_info:
                for interface in dbus_node_info.interfaces:
                    self._introspect_interface(interface, path)

        # TODO
        # indent(self._xml_node)

        if len(self._xml_node) < 1:
            return None
        xml_data = \
            '<!DOCTYPE node PUBLIC "-//freedesktop//DTD D-BUS Object ' \
            'Introspection 1.0//EN" "http://www.freedesktop.org/' \
            'standards/dbus/1.0/introspect.dtd">\n' + \
            ElementTree.tostring(
                self._xml_node, encoding='utf8', method='xml',
                xml_declaration=False).decode(encoding='utf8')
        # print(xml_data)
        self._info = Gio.DBusNodeInfo.new_for_xml(xml_data)
        if hasattr(self._info, "interfaces") and len(
                self._info.interfaces):
            for item in bus_proxy:
                self._create_dbus_proxy(item)

    def _create_dbus_proxy(self, item: tuple) -> None:
        self._dbus_proxy[item[0]] = Gio.DBusProxy.new_sync(
            self._bus, Gio.DBusCallFlags.NONE, self._info.interfaces[0],
            item[1], item[2], item[3], None)
        if item[4]:
            self._dbus_proxy[item[0]].connect("g-signal", item[4])

    def _on_signal(self, proxy: Gio.DBusProxy, sender_name: str,
                   signal_name: str, parameters: list) -> None:
        if proxy and sender_name:
            self._log("Signal", signal_name + ":", tuple(parameters))

    @staticmethod
    def boolean(value: bool):
        return GLib.Variant("b", value)

    @staticmethod
    def integer(value: bool):
        return GLib.Variant("i", value)
