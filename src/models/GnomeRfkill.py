# encoding: utf-8
#
# Bluetooth

from src.models.BaseDbus import BaseDbus


class GnomeRfkill(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {
            "airplane_mode": [None, []],
            "bluetooth_enabled": [None, []],
            "bluetooth_airplane_mode": [None, []]
        }
        self._dependence = {
            "set_airplane_mode": ["airplane_mode"],
            "get_airplane_mode": ["airplane_mode"],
            "min_airplane_mode": ["airplane_mode"],
            "max_airplane_mode": ["airplane_mode"],
            "list_airplane_mode": ["airplane_mode"],
            "set_bluetooth_enabled": ["bluetooth_airplane_mode"],
            "get_bluetooth_enabled": ["bluetooth_airplane_mode"],
            "min_bluetooth_enabled": ["bluetooth_airplane_mode"],
            "max_bluetooth_enabled": ["bluetooth_airplane_mode"],
            "list_bluetooth_enabled": ["bluetooth_airplane_mode"],
            "set_bluetooth_airplane_mode": ["bluetooth_airplane_mode"],
            "get_bluetooth_airplane_mode": ["bluetooth_airplane_mode"],
            "min_bluetooth_airplane_mode": ["bluetooth_airplane_mode"],
            "max_bluetooth_airplane_mode": ["bluetooth_airplane_mode"],
            "list_bluetooth_airplane_mode": ["bluetooth_airplane_mode"]
        }
        self._service_name = "org.gnome.SettingsDaemon.Rfkill"
        self._object_path = "/org/gnome/SettingsDaemon/Rfkill"
        self._interface_name = "org.gnome.SettingsDaemon.Rfkill"

    def init(self):
        self._create_dbus("session", (
            ("rfkill", self._service_name, self._object_path,
             self._interface_name, None),
            ("rfkill", self._service_name, self._object_path,
             "org.freedesktop.DBus.Properties", self._on_signal),))

    # Bluetooth enabled

    def set_bluetooth_enabled(self, value: bool) -> None:
        self.set_bluetooth_airplane_mode(not value)

    def get_bluetooth_enabled(self) -> bool:
        return not self.get_bluetooth_airplane_mode()

    # Airplane mode

    def set_airplane_mode(self, value: bool) -> None:
        self._luck["airplane_mode"](value)

    def get_airplane_mode(self) -> bool:
        return self._luck["airplane_mode"]()

    # Bluetooth airplane mode

    def set_bluetooth_airplane_mode(self, value: bool) -> None:
        self._luck["bluetooth_airplane_mode"](value)

    def get_bluetooth_airplane_mode(self) -> bool:
        return self._luck["bluetooth_airplane_mode"]()

    @staticmethod
    def min_on_off() -> int:
        return 0

    @staticmethod
    def max_on_off() -> int:
        return 1

    @staticmethod
    def list_on_off() -> dict:
        return {"0": "Off", "1": "On"}

    # Private

    def _airplane_mode(self, value: bool = None):
        if value is None:
            return self._dbus_proxy["rfkill"].Get(
                "(ss)", self._service_name, "AirplaneMode")
        else:
            self._dbus_proxy["rfkill"].Set(
                "(ssv)", self._service_name, "AirplaneMode",
                self.boolean(value))

    def _bluetooth_airplane_mode(self, value: bool = None):
        if value is None:
            return self._dbus_proxy["rfkill"].Get(
                "(ss)", self._service_name, "BluetoothAirplaneMode")
        else:
            self._dbus_proxy["rfkill"].Set(
                "(ssv)", self._service_name, "BluetoothAirplaneMode",
                self.boolean(value))

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        if proxy and sender_name and "PropertiesChanged" == signal_name:
            if parameters[0] == self._service_name:
                if "AirplaneMode" in parameters[1]:
                    v = parameters[1]["AirplaneMode"]
                    self._run_handlers("airplane_mode", v)
                if "BluetoothAirplaneMode" in parameters[1]:
                    v = parameters[1]["BluetoothAirplaneMode"]
                    self._run_handlers("bluetooth_airplane_mode", v)
                    self._run_handlers("bluetooth_enabled", not v)
