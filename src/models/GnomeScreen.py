# encoding: utf-8
#
# GnomeScreen
#

from src.models.BaseDbus import BaseDbus


class GnomeScreen(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {"brightness": [None, []]}
        self._dependence = {
            "set_brightness": ["brightness"],
            "get_brightness": ["brightness"],
            "min_brightness": ["brightness"],
            "max_brightness": ["brightness"],
            "list_brightness": ["brightness"]
        }
        self._service_name = "org.gnome.SettingsDaemon.Power"
        self._object_path = "/org/gnome/SettingsDaemon/Power"
        self._interface_name = "org.gnome.SettingsDaemon.Power.Screen"

        self._errors = []

        self._signal = "PropertiesChanged"
        self._property = "Brightness"

    def init(self):
        self._create_dbus("session", (
            ("power", self._service_name, self._object_path,
             self._interface_name, None),
            ("power", self._service_name, self._object_path,
             "org.freedesktop.DBus.Properties", self._on_signal),))

        if self.is_method("get_brightness"):
            self._log("Init Gnome screen")
            self._log("    Brightness:", self.get_brightness())

    # Brightness

    def set_brightness(self, value: int) -> None:
        return self._luck["brightness"](value)

    def get_brightness(self) -> int:
        return self._luck["brightness"]()

    @staticmethod
    def min_brightness() -> int:
        return 0

    @staticmethod
    def max_brightness() -> int:
        return 100

    def list_brightness(self) -> dict:
        data = {}
        max_value = self.max_brightness()
        if max_value:
            for i in range(0, max_value + 1):
                data[i] = str(i)
        return data

    # Private

    def _brightness(self, value=None):
        if value is None:
            try:
                value = self._dbus_proxy["power"].Get(
                    "(ss)", self._interface_name, self._property)
            except Exception as e:
                self._error_exception(e)
                value = 0
            return value
        else:
            try:
                self._dbus_proxy["power"].Set(
                    "(ssv)", self._interface_name, self._property,
                    self.integer(value))
            except Exception as e:
                self._error_exception(e)

    def _on_signal(self, _proxy, _sender_name, signal_name, parameters):
        if self._signal == signal_name:
            if parameters[0] == self._interface_name:
                if self._property in parameters[1]:
                    value = int(parameters[1][self._property])
                    self._run_handlers("brightness", value)

    def _error_exception(self, e):
        # TODO User click pkexec cancel
        code = -1
        message = ""
        if hasattr(e, "code"):
            code = e.code
        if hasattr(e, "message"):
            message = e.message
        print(code, message)
        self._errors.append((code, message))
