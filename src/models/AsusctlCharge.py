# encoding: utf-8
#
# AsusctlCharge
#

from src.models.BaseDbus import BaseDbus


class AsusctlCharge(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {"chg_limit": [None, []]}
        self._dependence = {
            "set_chg_limit": ["set_limit"],
            "get_chg_limit": ["limit", "set_limit"],
            "min_chg_limit": ["limit"],
            "max_chg_limit": ["limit"],
            "list_chg_limit": ["limit"]
        }
        self._service_name = "org.asuslinux.Daemon"
        self._object_path = "/org/asuslinux/Charge"
        self._interface_name = "org.asuslinux.Daemon"

    def init(self):
        self._create_dbus("system", (
            ("chg", self._service_name, self._object_path,
             self._interface_name, self._on_signal),))

    # Battery charge limit

    def set_chg_limit(self, value: int) -> None:
        self._luck["set_limit"](value)

    def get_chg_limit(self) -> int:
        return self._luck["limit"]()

    @staticmethod
    def min_chg_limit() -> int:
        return 20

    @staticmethod
    def max_chg_limit() -> int:
        return 100

    def list_chg_limit(self) -> dict:
        data = {}
        for i in range(self.min_chg_limit(), self.max_chg_limit() + 1):
            data[i] = str(i)
        return data

    # Private

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        if proxy and sender_name:
            params = tuple(parameters)
            if signal_name == "NotifyCharge":
                self._run_handlers("chg_limit", params[0])
            else:
                self._log("Signal", signal_name + ":", params[0])

    def _limit(self):
        return self._dbus_proxy["chg"].Limit()

    def _set_limit(self, limit):
        self._dbus_proxy["chg"].SetLimit("(y)", limit)
