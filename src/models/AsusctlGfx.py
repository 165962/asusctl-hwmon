# encoding: utf-8
#
# AsusctlGfx
#

from src.models.BaseDbus import BaseDbus


class AsusctlGfx(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {"gfx_mode": [None, []]}
        self._dependence = {
            "set_gfx_mode": ["set_vendor"],
            "get_gfx_mode": ["vendor", "set_vendor"],
            "min_gfx_mode": ["vendor"],
            "max_gfx_mode": ["vendor"],
            "list_gfx_mode": ["vendor"],
        }
        self._service_name = "org.asuslinux.Daemon"
        self._object_path = "/org/asuslinux/Gfx"
        self._interface_name = "org.asuslinux.Daemon"

    def init(self):
        self._create_dbus("system", (
            ("gfx", self._service_name, self._object_path,
             self._interface_name, self._on_signal),))

        # if self.is_method("power"):
        #    print("    power:", self._luck["power"]())
        # supported = self._dbus_proxy["spd"].SupportedFunctions()
        # print("supported: ", supported)
        # supported: (False, (True,), (True, False, True),
        # (True, [], False, False), (True, False))
        #   "gfx_mode": "Hybrid",
        #   "gfx_managed": true,
        #   "gfx_vfio_enable": false,
        # exit(0)

    # Graphics mode

    def set_gfx_mode(self, value) -> None:
        return self._luck["set_vendor"](value)

    def get_gfx_mode(self) -> int:
        return self._luck["vendor"]()

    @staticmethod
    def min_gfx_mode() -> int:
        return 0

    def max_gfx_mode(self) -> int:
        return len(self.list_gfx_mode()) - 1

    @staticmethod
    def list_gfx_mode() -> dict:
        return {0: "nvidia", 1: "Integrated", 2: "Compute", 4: "Hybrid"}

    # Private

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        if proxy and sender_name:
            params = tuple(parameters)
            if signal_name == "NotifyGfx":
                vendor = self._luck["vendor"]()
                self._run_handlers("gfx_mode", vendor)

                # TODO
                old = self.get_check("gfx_mode")
                self._log(signal_name, old, ">", vendor, "<", params[0])

            else:
                self._log("Signal", signal_name + ":", params[0])

    def _power(self):
        return self._dbus_proxy["gfx"].Power()

    def _vendor(self):
        return self._dbus_proxy["gfx"].Vendor()

    def _set_vendor(self, value):
        self._dbus_proxy["gfx"].SetVendor("(u)", value)
