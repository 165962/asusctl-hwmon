# encoding: utf-8
#
# NetworkManager
#
# cli: nmcli
#

from src.cmd import cmd
from src.models.BaseDbus import BaseDbus


class NetworkManager(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {
            "wireless_enabled": [None, []],
            "wireless_power_save": [None, []]
        }
        self._dependence = {
            "get_version": ["get_version"],
            "set_wireless_enabled": ["wireless_enabled"],
            "get_wireless_enabled": ["wireless_enabled"],
            "min_wireless_enabled": ["wireless_enabled"],
            "max_wireless_enabled": ["wireless_enabled"],
            "list_wireless_enabled": ["wireless_enabled"],
            "get_wireless_devices": ["get_version"],
            "get_wireless_auto_connections": ["get_version"],
            "set_wireless_power_save": ["get_version"],
            "get_wireless_power_save": ["get_version"],
            "min_wireless_power_save": ["get_version"],
            "max_wireless_power_save": ["get_version"],
            "list_wireless_power_save": ["get_version"],
        }
        self._service_name = "org.freedesktop.NetworkManager"
        self._object_path = "/org/freedesktop/NetworkManager"
        self._interface_name = "org.freedesktop.NetworkManager"

    def init(self):
        self._create_dbus("system", (
            ("nm", self._service_name, self._object_path,
             self._interface_name, None),
            ("nm", self._service_name, self._object_path,
             "org.freedesktop.DBus.Properties", self._on_signal),))

        if self.is_method("get_version"):
            self._log("Init NetworkManager")
            self._log("    Version:", self._luck["get_version"]())
            if self.is_method("get_wireless_enabled"):
                enabled = self.get_wireless_enabled()
                self._log("    Wireless enabled:", enabled)
            if self.is_method("get_wireless_devices"):
                devices = self.get_wireless_devices()
                self._log("    Wireless devices:", devices)

    @staticmethod
    def get_wireless_devices() -> list:
        devices = []
        command = "nmcli -t -c no -f device,type d"
        for item in cmd(command).split("\n"):
            device = item.split(":")
            if device[1] == "wifi":
                devices.append(device[0])
        return devices

    @staticmethod
    def get_wireless_auto_connections():
        connections = []
        for item in cmd("nmcli -c no -t -f name,type c").split("\n"):
            connection = item.split(":")
            if connection[1] == "802-11-wireless":
                c = "nmcli -c no -t -f connection.autoconnect c s {con}"
                output = cmd(c.format(con=connection[0]))
                if output == "connection.autoconnect:yes":
                    connections.append(connection[0])
        return connections

    # NetworkManager wireless enabled.

    def set_wireless_enabled(self, value) -> None:
        self._luck["wireless_enabled"](value)

    def get_wireless_enabled(self) -> bool:
        return self._luck["wireless_enabled"]()

    @staticmethod
    def min_enabled() -> int:
        return 0

    @staticmethod
    def max_enabled() -> int:
        return 1

    @staticmethod
    def list_enabled() -> dict:
        return {"0": "Off", "1": "On"}

    # NetworkManager wireless power save.

    def set_wireless_power_save(self, value) -> None:
        connections = self.get_wireless_auto_connections()
        if len(connections) > 0:
            if self.get_wireless_enabled():
                self.set_wireless_enabled(False)
            command = "nmcli con mod {connection} " \
                      "802-11-wireless.powersave {mode}"
            cmd(command.format(connection=connections[0], mode=value))
            self.set_wireless_enabled(True)

    def get_wireless_power_save(self) -> int:
        value = -1
        connections = self.get_wireless_auto_connections()
        if len(connections) > 0:
            command = "nmcli -t -c no -f 802-11-wireless.powersave c " \
                      "s {connection}".format(connection=connections[0])
            output = cmd(command)[26:]
            data = {"default": 0, "ignore": 1, "disable": 2, "enable": 3}
            for key, mode in data.items():
                if key == output:
                    value = mode
                    break
        return value

    @staticmethod
    def min_wireless_power_save() -> int:
        return 0

    def max_wireless_power_save(self) -> int:
        return len(self.list_wireless_power_save()) - 1

    @staticmethod
    def list_wireless_power_save() -> dict:
        return {0: "Default", 1: "Ignore", 2: "Disable", 3: "Enable"}

    # Private

    def _get_version(self):
        return self._dbus_proxy["nm"].Get(
            "(ss)", self._interface_name, "Version")

    def _wireless_enabled(self, value=None):
        if value is None:
            return self._dbus_proxy["nm"].Get(
                "(ss)", self._service_name, "WirelessEnabled")
        else:
            self._dbus_proxy["nm"].Set(
                "(ssv)", self._service_name, "WirelessEnabled",
                self.boolean(value))

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        if proxy and sender_name and "PropertiesChanged" == signal_name:
            if parameters[0] == self._service_name:
                if "WirelessEnabled" in parameters[1]:
                    enabled = parameters[1]["WirelessEnabled"]
                    self._run_handlers("wireless_enabled", enabled)
