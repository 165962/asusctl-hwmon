# encoding: utf-8
#
# AsusctlGfx
#

from src.models.BaseDbus import BaseDbus


class AsusctlLed(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {"kbd_bright": [None, []]}
        self._dependence = {
            "set_kbd_bright": ["set_kbd_bright"],
            "get_kbd_bright": ["get_kbd_bright", "set_kbd_bright"],
            "min_kbd_bright": ["get_kbd_bright"],
            "max_kbd_bright": ["get_kbd_bright"],
            "list_kbd_bright": ["get_kbd_bright"],
        }
        self._service_name = "org.asuslinux.Daemon"
        self._object_path = "/org/asuslinux/Led"
        self._interface_name = "org.asuslinux.Daemon"

    def init(self):
        self._create_dbus("system", (
            ("led", self._service_name, self._object_path,
             self._interface_name, self._on_signal),
            ("ldp", self._service_name, self._object_path,
             "org.freedesktop.DBus.Properties", None),))

    # Keyboard brightness

    def set_kbd_bright(self, value: int) -> None:
        self._luck["set_kbd_bright"](value)
        self._run_handlers("kbd_bright", self.get_kbd_bright())

    def _get_kbd_bright(self) -> int:
        try:
            value = self._luck["get_kbd_bright"]()
        except Exception as e:
            value = 0

        # TODO x - 48
        if value - 48 >= 0:
            value = value - 48
        else:
            self._log("Asusctl::_get_kbd_bright dbus return", value)

        return value

    # TODO
    def get_kbd_bright(self) -> int:
        value = -1
        i = 0
        while value < 0:
            value = self._get_kbd_bright()
            if i > 9:
                self._log("ERROR: Asusctl::get_kbd_bright return", value)
                break
            i = i + 1
        return value

    @staticmethod
    def min_kbd_bright() -> int:
        return 0

    def max_kbd_bright(self) -> int:
        return len(self.list_kbd_bright()) - 1

    @staticmethod
    def list_kbd_bright() -> dict:
        return {0: "Оff", 1: "Low", 2: "Medium", 3: "High"}

    # Private

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        if proxy and sender_name:
            if signal_name == "NotifyLed":
                pass
            else:
                self._log("Signal", signal_name + ":", tuple(parameters))

    def _led_brightness(self):
        return self._dbus_proxy["ldp"].Get("(ss)", self._interface_name,
                                           "LedBrightness")

    def _set_brightness(self, value):
        self._dbus_proxy["led"].SetBrightness("(u)", value)
