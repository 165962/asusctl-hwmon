# encoding: utf-8
#


class BaseModel:
    def __init__(self, *args):
        self._handlers = {}

    def is_method(self, name):
        return True

    @staticmethod
    def _log(*args):
        print(*args)

    def _run_handlers(self, key, value):
        if key in self._handlers:
            for callback in self._handlers[key][1]:
                callback(value)

    @staticmethod
    def get_test():
        print("get test")
        return 0

    def get_check(self, key):
        return self._handlers[key][0]

    def set_check(self, key, value):
        self._handlers[key][0] = value

    def add_handler(self, listener, callback):
        if listener in self._handlers:
            self._handlers[listener][1].append(callback)

    def run_handlers(self, listener, value):
        self._run_handlers(listener, value)
