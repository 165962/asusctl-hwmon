# encoding: utf-8
#
# Bluetooth
#
# TODO GDBus.Error:org.bluez.Error.Blocked:
#      Blocked through rfkill (36) False

from src.models.BaseDbus import BaseDbus


class Bluetooth(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {"powered": [None, []]}
        self._service_name = "org.bluez"
        self._object_path = "/org/bluez/hci0"
        self._interface_name = "org.bluez.Adapter1"
        self._dependence = {
            "set_powered": ["powered"],
            "get_powered": ["powered"],
            "min_powered": ["powered"],
            "max_powered": ["powered"],
            "list_powered": ["powered"]
        }

    def init(self):
        self._create_dbus("system", (
            ("bluez", self._service_name, self._object_path,
             self._interface_name, None),
            ("bluez", self._service_name, self._object_path,
             "org.freedesktop.DBus.Properties", self._on_signal),))

        self._log("Init Bluetooth")
        if self.is_method("get_powered"):
            self._log("    Powered:", self.get_powered())

    # Powered

    def get_powered(self) -> bool:
        return self._luck["powered"]()

    def set_powered(self, value: bool) -> None:
        return self._luck["powered"](value)

    @staticmethod
    def min_powered() -> int:
        return 0

    @staticmethod
    def max_powered() -> int:
        return 1

    @staticmethod
    def list_powered() -> dict:
        return {"0": "Off", "1": "On"}

    # Private

    def _powered(self, value=None):
        if value is None:
            return self._dbus_proxy["bluez"].Get(
                "(ss)", "org.bluez.Adapter1", "Powered")
        else:
            try:
                self._dbus_proxy["bluez"].Set(
                    "(ssv)", "org.bluez.Adapter1", "Powered",
                    self.boolean(value))
            except Exception as e:
                print("Exception bluez", e, self.get_powered())

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        if proxy and sender_name and "PropertiesChanged" == signal_name:
            params = tuple(parameters)
            if params[0] == "org.bluez.Adapter1":
                if "Powered" in params[1]:
                    self._run_handlers("powered", params[1]["Powered"])
