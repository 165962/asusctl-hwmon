# encoding: utf-8
#
# Nvidia
#
# cli: nvidia-smi

from src.cmd import cmd


class Nvidia:
    def __init__(self):
        super().__init__()
        # gpu_name, vbios_version, driver_version, clocks.sm, clocks.mem,
        # utilization.gpu, utilization.memory, power.draw
        # temperature.gpu, pstate
        self._abbr = {
            "edge": "temperature.gpu",
            "sclk": "clocks.sm",
            "ugpu": "utilization.gpu",
            "umem": "utilization.memory",
            "power": "power.draw"
        }
        self._methods = {}

        # TODO
        self.init()

    def init(self):
        gpu_name = self.gpu_name()

        # TODO
        if gpu_name:
            self._methods["gpu_name"] = True
            self._methods["gpu_short_name"] = True
            self._methods["gpu_data"] = True

    def is_method(self, name: str):
        if name in self._methods:
            return True

    def gpu_name(self):
        data = self.query_gpu(["gpu_name"])
        return data[0]["gpu_name"]

    def gpu_short_name(self):
        return self.gpu_name() \
            .replace("GeForce ", "") \
            .replace("RTX ", "RTX") \
            .replace("with ", "") \
            .replace(" Max-Q Design", "").lower()

    def gpu_data(self):
        data = self.query_gpu(["umem", "ugpu", "power", "edge"])
        return data[0]

    def query_gpu(self, units):
        args = []
        for unit in units:
            if unit in self._abbr:
                args.append(self._abbr[unit])
            else:
                args.append(unit)
        query = "nvidia-smi --query-gpu={units} --format=csv,noheader"
        output = cmd(query.format(units=",".join(args)))
        data = []
        for y, line in enumerate(output.split("\n")):
            data.append({})
            for x, value in enumerate(line.split(", ")):
                data[y][units[x]] = value
        return data
