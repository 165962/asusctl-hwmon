# encoding: utf-8
#
# Fan
# cli: sh, echo, cat, pkexec
#

from os import access, EX_OK
from os.path import isfile

from src.models.BaseModel import BaseModel


class Fan(BaseModel):
    def __init__(self, path):
        super().__init__()
        self._handlers = {"mode": [None, []]}
        self._methods = {}
        self._path = path

    def init(self):
        name = self._read("name")
        label = self._read("fan1_label")

        file = "/usr/bin/pkexec"

        if self._path and name and label and isfile(file) and \
                access(file, EX_OK):

            # TODO
            self._methods["get_input"] = True
            self._methods["get_mode"] = True
            self._methods["min_mode"] = True
            self._methods["max_mode"] = True
            self._methods["list_mode"] = True
            self._methods["command_set_mode"] = True

            self._log("Init Fan")
            self._log("    Path:", self._path)
            self._log("    Name:", name)
            self._log("    Label:", label)

    def is_method(self, name):
        if name in self._methods:
            return True

    def get_input(self) -> str:
        return self._read("fan1_input")

    def get_mode(self) -> bool:
        mode = self._read("pwm1_enable")
        if mode == "0":
            return False
        elif mode == "2":
            return True
        else:
            # TODO Error fan
            print("ERROR: Fan mode error", mode)

    @staticmethod
    def min_mode() -> int:
        return 0

    @staticmethod
    def max_mode() -> int:
        return 1

    @staticmethod
    def list_mode() -> dict:
        return {"0": "Off", "1": "On"}

    def command_set_mode(self, value) -> list:
        value = self._mode(value)
        return [
            "/usr/bin/pkexec", "/bin/sh", "-c",
            "echo {0} | tee {1}/pwm1_enable".format(value, self._path)
        ]

    # Private

    def _read(self, path):
        data = None
        if self._path != "":
            with open("{0}/{1}".format(self._path, path), "r") as fp:
                data = fp.read().strip()
        return data

    @staticmethod
    def _mode(value) -> str:
        if isinstance(value, bool):
            if value:
                return "2"
            else:
                return "0"
