# encoding: utf-8
#
# AsusctlProfile
#
# cli: asusctl
#

import json

from src.models.BaseDbus import BaseDbus


class AsusctlProfile(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {
            "profile": [None, []],
            "profile_turbo": [None, []],
            "profile_fan_preset": [None, []],
            "profile_fan_curve": [None, []]
        }
        self._dependence = {
            "profiles": ["profiles"],

            "set_profile": ["set_profile"],

            "get_profile": ["active_name", "active_data", "set_profile"],
            # "min_profile": [],
            # "max_profile": [],
            "list_profile": ["active_name", "active_data"],

            "next_profile": ["next_profile"],

            "create_profile": ["new_or_modify"],

            "remove_profile": ["remove_profile"],

            "get_profile_turbo": ["active_data"],
            # "min_profile_turbo": ["active_data"],
            # "max_profile_turbo": ["active_data"],
            # "list_profile_turbo": [],

            "set_profile_fan_curve": ["active_data"],

            "get_profile_fan_curve": ["active_data"],

            "get_profile_fan_preset": ["active_data"],
            # "min_profile_fan_preset": [],
            # "max_profile_fan_preset": [],
            "list_profile_fan_preset": ["active_data"]
        }
        self._service_name = "org.asuslinux.Daemon"
        self._object_path = "/org/asuslinux/Profile"
        self._interface_name = "org.asuslinux.Daemon"

    def init(self):
        self._create_dbus("system", (
            ("prl", self._service_name, self._object_path,
             self._interface_name, self._on_signal),))

    # Profile

    def set_profile(self, value: int) -> None:
        profiles = self.list_profile()
        profile = profiles[value]
        self._luck["set_profile"](profile)

    def get_profile(self) -> int:
        value = -1
        profiles = self.list_profile()
        for key, name in profiles.items():
            if name == self._luck["active_name"]():
                value = key
        return value

    @staticmethod
    def min_profile() -> int:
        return 0

    def max_profile(self) -> int:
        return len(self.list_profile()) - 1

    def list_profile(self) -> dict:
        data = {}
        profile_names = self._profile_names()
        for i in range(len(profile_names)):
            data[i] = profile_names[i]
        return data

    def next_profile(self):
        self._luck["next_profile"]()

    def create_profile(self, profile: str) -> None:
        data = [profile, 0, 0, False, 0, '']
        self._luck["new_or_modify"](data)

    def remove_profile(self, value: int) -> None:
        profile = self.list_profile()[value]
        if profile == self._luck["active_name"]():
            self.next_profile()
        self._luck["remove_profile"](profile)

    # Profile turbo

    def set_profile_turbo(self, value: int) -> None:
        # TODO
        # mode = ("normal", "boost", "silent")[value]
        data = self._luck["active_data"]()
        data["turbo"] = value
        n = []
        for key in data:
            n.append(data[key])
        self._luck["new_or_modify"](n)

    def get_profile_turbo(self) -> int:
        return int(self._luck["active_data"]()["turbo"])

    @staticmethod
    def min_profile_turbo() -> int:
        return 0

    def max_profile_turbo(self) -> int:
        return len(self.list_profile_turbo()) - 1

    @staticmethod
    def list_profile_turbo() -> dict:
        return {0: "Off", 1: "On"}

    # Profile fan preset

    def set_profile_fan_preset(self, value: int) -> None:
        # TODO
        # mode = ("normal", "boost", "silent")[value]
        data = self._luck["active_data"]()
        data["fan_preset"] = value
        n = []
        for key in data:
            n.append(data[key])
        self._luck["new_or_modify"](n)

    def get_profile_fan_preset(self) -> int:
        return self._luck["active_data"]()["fan_preset"]

    @staticmethod
    def min_profile_fan_preset() -> int:
        return 0

    def max_profile_fan_preset(self) -> int:
        return len(self.list_profile_fan_preset()) - 1

    @staticmethod
    def list_profile_fan_preset() -> dict:
        return {0: "Normal", 1: "Boost", 2: "Silent"}

    # Profile fan curve

    def set_profile_fan_curve(self, value: str) -> None:
        data = self._luck["active_data"]()
        data["fan_curve"] = value
        n = []
        for key in data:
            n.append(data[key])
        self._luck["new_or_modify"](n)

    def get_profile_fan_curve(self):
        return self._luck["active_data"]()["fan_curve"]

    # Private

    def _new_or_modify(self, data):
        self._dbus_proxy["prl"].NewOrModify("((syybus))", data)

    def _active_name(self):
        return self._dbus_proxy["prl"].ActiveName()

    def _active_profile_name(self):
        return self._dbus_proxy["prl"].ActiveProfileName()

    def _profile(self):
        try:
            data = json.loads(self._dbus_proxy["prl"].Profile())
        except Exception as e:
            self._log("Exception AsusctlProfiles->_profile", e)
            data = {
                "profile": "",
                "min_percentage": 0,
                "max_percentage": 0,
                "turbo": False,
                "fan_preset": 0,
                "fan_curve": ''
            }
        return data

    def _active_data(self):
        try:
            data = self._dbus_proxy["prl"].ActiveData()
        except Exception as e:
            self._log("Exception AsusctlProfiles->_active_data", e)
            data = ('', 0, 0, False, 0, '')
        return {
            "profile": data[0],
            "min_percentage": data[1],
            "max_percentage": data[2],
            "turbo": data[3],
            "fan_preset": data[4],
            "fan_curve": data[5]
        }

    def _set_profile(self, profile):
        self._dbus_proxy["prl"].SetProfile("(s)", profile)

    def _next_profile(self):
        self._dbus_proxy["prl"].NextProfile()

    def _remove_profile(self, profile: str):
        self._dbus_proxy["prl"].Remove("(s)", profile)

    def _profile_names(self):
        try:
            profile_names = self._dbus_proxy["prl"].ProfileNames()
        except Exception as e:
            self._log("Exception Asusctl->list_profile", e)
            profile_names = []
        return profile_names

    def _profiles(self):
        return self._dbus_proxy["prl"].Profiles()

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        if proxy and sender_name:
            params = tuple(parameters)
            if signal_name == "NotifyProfile":
                value = self.get_profile()
                if value != self.get_check("profile"):
                    self._run_handlers("profile", value)
                value = self.get_profile_turbo()
                if value != self.get_check("profile_turbo"):
                    self._run_handlers("profile_turbo", value)
                value = self.get_profile_fan_preset()
                if value != self.get_check("profile_fan_preset"):
                    self._run_handlers("profile_fan_preset", value)
            else:
                self._log("Signal", signal_name + ":", params[0])
