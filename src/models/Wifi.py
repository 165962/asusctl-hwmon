# encoding: utf-8
#
# WiFi
#
# cli: pkexec, iw, awk
#

from os import access, EX_OK
from os.path import isfile

from src.cmd import cmd
from src.models.BaseModel import BaseModel


class Wifi(BaseModel):
    def __init__(self):
        super().__init__()
        self._handlers = {
            "powered": [None, []],
            "power_save": [None, []]
        }
        self._methods = {}

    def init(self):
        command1 = 'LANG=en_IN.UTF-8 /sbin/iw --version'
        output1 = cmd(command1)

        file = "/usr/bin/pkexec"

        if output1 and isfile(file) and access(file, EX_OK):

            # TODO
            self._methods["get_faces"] = True
            self._methods["get_power_save"] = True
            self._methods["min_power_save"] = True
            self._methods["max_power_save"] = True
            self._methods["list_power_save"] = True
            self._methods["command_set_power_save"] = True

            self._log('Init Wi-Fi')
            self._log('    ' + output1)
            self._log("    Face:", self.get_faces()) # wlp2s0

    def is_method(self, name):
        if name in self._methods:
            return True

    @staticmethod
    def get_faces() -> str:
        command = "/sbin/iw dev | awk '$1==\"Interface\"{print $2}'"
        output = cmd(command)
        return output

    def get_power_save(self) -> bool:
        command = "/sbin/iw dev {device} get power_save"\
            .format(device=self.get_faces())
        output = cmd(command)
        if output[-4:] == ": on":
            return True
        elif output[-5:] == ": off":
            return False

    @staticmethod
    def min_power_save() -> int:
        return 0

    @staticmethod
    def max_power_save() -> int:
        return 1

    @staticmethod
    def list_power_save() -> dict:
        return {"0": "Off", "1": "On"}

    def command_set_power_save(self, value) -> list:
        if value:
            value = "on"
        else:
            value = "off"
        return ["/usr/bin/pkexec", "/sbin/iw", "dev", self.get_faces(),
                "set", "power_save", value]
