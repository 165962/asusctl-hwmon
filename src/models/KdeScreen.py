# encoding: utf-8
#
# Screen
#

from src.models.BaseDbus import BaseDbus


class KdeScreen(BaseDbus):
    def __init__(self, *args):
        super().__init__(self, *args)
        self._handlers = {"brightness": [None, []]}
        self._dependence = {
            "set_brightness": ["set_brightness"],
            "get_brightness": ["get_brightness", "set_brightness"],
            "min_brightness": ["get_brightness"],
            "max_brightness": ["max_brightness", "get_brightness"],
            "list_brightness": ["max_brightness", "get_brightness"]
        }
        self._service_name =\
            "org.kde.Solid.PowerManagement"
        self._object_path =\
            "/org/kde/Solid/PowerManagement/Actions/BrightnessControl"
        self._interface_name =\
            "org.kde.Solid.PowerManagement.Actions.BrightnessControl"
        self._signal = "brightnessChanged"
        self._errors = []

    def init(self):
        self._create_dbus("session", (
            ("kde", self._service_name, self._object_path,
             self._interface_name, self._on_signal),))

        if self.is_method("get_brightness"):
            self._log("Init KDE screen")
            self._log("    Brightness:", self.get_brightness())
            self._log("    Max brightness:", self.max_brightness())

    # Brightness

    def set_brightness(self, value: int) -> None:
        return self._luck["set_brightness"](value)

    def get_brightness(self) -> int:
        return self._luck["get_brightness"]()

    @staticmethod
    def min_brightness() -> int:
        return 0

    def max_brightness(self) -> int:
        return self._luck["max_brightness"]()

    def list_brightness(self) -> dict:
        data = {}
        max_value = self.max_brightness()
        if max_value:
            for i in range(0, max_value + 1):
                data[i] = str(i)
        return data

    # Private

    def _set_brightness(self, value):
        self._dbus_proxy['kde'].setBrightness("(i)", value)

    def _get_brightness(self):
        return self._dbus_proxy['kde'].brightness()

    def _max_brightness(self):
        return self._dbus_proxy['kde'].brightnessMax()

    def _on_signal(self, _proxy, _sender_name, signal_name, parameters):
        if self._signal == signal_name:
            value = int(parameters[0])
            self._run_handlers("brightness", value)

    def _error_exception(self, e):
        # TODO User click pkexec cancel
        code = -1
        message = ""
        if hasattr(e, "code"):
            code = e.code
        if hasattr(e, "message"):
            message = e.message
        print(code, message)
        self._errors.append((code, message))
