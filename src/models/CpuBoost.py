# encoding: utf-8
#
# CpuBoost
#
# cli: sh, cat,

from os import access, EX_OK
from os.path import isfile
from src.cmd import cmd

path = "/sys/devices/system/cpu/cpufreq/boost"
data = {"0": "Off", "1": "On"}


class CpuBoost:
    def __init__(self):
        super().__init__()
        self._methods = {}

    def init(self):
        file = "/usr/bin/pkexec"

        if isfile(file) and access(file, EX_OK):

            # TODO
            self._methods["get_boost"] = True
            self._methods["set_boost"] = True
            self._methods["min_boost"] = True
            self._methods["max_boost"] = True
            self._methods["list_boost"] = True
            self._methods["command_set_boost"] = True

    @staticmethod
    def get_boost() -> bool:
        command = "cat " + path
        output = cmd(command)
        if output == "1":
            return True
        elif output == "0":
            return False

    @staticmethod
    def min_boost() -> int:
        return 0

    @staticmethod
    def max_boost() -> int:
        return 1

    @staticmethod
    def list_boost() -> dict:
        return data

    @staticmethod
    def command_set_boost(value) -> list:
        if value:
            value = "1"
        else:
            value = "0"
        command = "echo " + value + " | tee " + path
        return ["/usr/bin/pkexec", "/bin/sh", "-c", command]
