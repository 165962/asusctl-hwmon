# encoding: utf-8
#
# Sound
# cli: amixer

import subprocess

from src.models.BaseModel import BaseModel

data_on = {"0": "Off", "1": "On"}


class Sound(BaseModel):
    def __init__(self):
        super().__init__()
        self._handlers = {
            "master_on": [None, []], "master_volume": [None, []],
            "capture_on": [None, []], "capture_volume": [None, []]}
        self._methods = {}

    def init(self):
        amixer = self._cmd("amixer --version")

        if amixer:

            # TODO
            self._methods["set_master_on"] = True
            self._methods["get_master_on"] = True
            self._methods["min_master_on"] = True
            self._methods["max_master_on"] = True
            self._methods["list_master_on"] = True
            self._methods["command_set_master_on"] = True
            self._methods["set_master_volume"] = True
            self._methods["get_master_volume"] = True
            self._methods["min_master_volume"] = True
            self._methods["max_master_volume"] = True
            self._methods["list_master_volume"] = True
            self._methods["command_set_master_volume"] = True
            self._methods["set_capture_on"] = True
            self._methods["get_capture_on"] = True
            self._methods["min_capture_on"] = True
            self._methods["max_capture_on"] = True
            self._methods["list_capture_on"] = True
            self._methods["command_set_capture_on"] = True
            self._methods["set_capture_volume"] = True
            self._methods["get_capture_volume"] = True
            self._methods["min_capture_volume"] = True
            self._methods["max_capture_volume"] = True
            self._methods["list_capture_volume"] = True
            self._methods["command_set_capture_volume"] = True

            master_on = self.str_on(self.get_master_on())
            master_volume = self.str_volume(self.get_master_volume())
            capture_on = self.str_on(self.get_capture_on())
            capture_volume = self.str_volume(self.get_capture_volume())
            self._log("Init Sound")
            self._log("   ", amixer)
            self._log("    Master", master_on, master_volume)
            self._log("    Capture", capture_on, capture_volume)

    def is_method(self, name):
        if name in self._methods:
            return True

    @staticmethod
    def _cmd(command=None):
        proc = subprocess.Popen(command, shell=True,
                                stdout=subprocess.PIPE)
        stdout = proc.communicate()[0]
        proc.wait()
        return stdout.decode("utf-8").strip()

    @staticmethod
    def _content_find(content: str, find_start: str, find_end: str):
        value = False
        length = len(find_start)
        for line in content.split("\n"):
            start = line.find(find_start)
            if start > -1:
                end = line.find(find_end, start + length)
                if end > -1:
                    v = line[start + length:end]
                    # if value and value != v:
                    #     print("Warning alsa mixer find", value, v)
                    value = v
        return value

    @staticmethod
    def get_list() -> dict:
        data = {}
        max_value = 100
        if max_value:
            for i in range(0, max_value + 1):
                data[i] = str(i)
        return data

    def str_on(self, value):
        if value:
            string = "On"
        else:
            string = "Off"
        return string

    def str_volume(self, value):
        return "{}%".format(value)

    def get_master_on(self):
        state = None
        content = self._cmd(self.command_get_master_on())
        value = self._content_find(content, "%] [", "]")
        if value == "on":
            state = True
        elif value == "off":
            state = False
        return state

    @staticmethod
    def min_master_on() -> int:
        return 0

    @staticmethod
    def max_master_on() -> int:
        return 1

    @staticmethod
    def list_master_on() -> dict:
        return data_on

    def get_master_volume(self):
        content = self._cmd(self.command_get_master_volume())
        percent = self._content_find(content, "[", "%]")
        return int(percent)

    @staticmethod
    def min_master_volume() -> int:
        return 0

    @staticmethod
    def max_master_volume() -> int:
        return 100

    def list_master_volume(self) -> dict:
        return self.get_list()

    def get_capture_on(self):
        state = None
        content = self._cmd(self.command_get_capture_on())
        value = self._content_find(content, "%] [", "]")
        if value == "on":
            state = True
        elif value == "off":
            state = False
        return state

    @staticmethod
    def min_capture_on() -> int:
        return 0

    @staticmethod
    def max_capture_on() -> int:
        return 1

    @staticmethod
    def list_capture_on() -> dict:
        return data_on

    def get_capture_volume(self):
        content = self._cmd(self.command_get_capture_volume())
        percent = self._content_find(content, "[", "%]")
        return int(percent)

    @staticmethod
    def min_capture_volume() -> int:
        return 0

    @staticmethod
    def max_capture_volume() -> int:
        return 100

    def list_capture_volume(self) -> dict:
        return self.get_list()

    @staticmethod
    def command_get_master_on() -> list:
        return ["/usr/bin/amixer sget Master"]

    @staticmethod
    def command_get_master_volume() -> list:
        return ["/usr/bin/amixer sget Master"]

    @staticmethod
    def command_get_capture_on() -> list:
        return ["/usr/bin/amixer sget Capture"]

    @staticmethod
    def command_get_capture_volume() -> list:
        return ["/usr/bin/amixer sget Capture"]

    @staticmethod
    def command_set_master_on(value) -> list:
        if value:
            a = "unmute"
        else:
            a = "mute"
        return ["/usr/bin/amixer", "sset", "Master", a]

    @staticmethod
    def command_set_capture_on(value) -> list:
        if value:
            a = "cap"
        else:
            a = "nocap"
        return ["/usr/bin/amixer", "sset", "Capture", a]

    @staticmethod
    def command_set_master_volume(value) -> list:
        percent = float(int(value))
        return ["/usr/bin/amixer", "sset", "Master", str(percent) + "%"]

    @staticmethod
    def command_set_capture_volume(value) -> list:
        percent = float(int(value))
        return ["/usr/bin/amixer", "sset", "Capture", str(percent) + "%"]
