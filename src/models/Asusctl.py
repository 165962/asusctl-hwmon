# encoding: utf-8
#
# Asusctl
#
# cli: asusctl
#
# dbus-send --system --type=method_call --print-reply
# --dest=org.asuslinux.Daemon /org/asuslinux/Gfx
# org.freedesktop.DBus.Introspectable.Introspect
#
# asus_kbd_backlight = "/sys/class/leds/asus::kbd_backlight/brightness"
# asusd_config = "/etc/asusd/asusd.conf"
# with open(asusd_config, "r", encoding='utf-8') as f:
#     data = json.load(f)
# print(json.dumps(self._spec, ensure_ascii=False, indent=4))

from gi.repository import Gio

from src.cmd import cmd
from src.models.BaseDbus import BaseDbus


class Asusctl(BaseDbus):
    def __init__(self):
        super().__init__()
        self._handlers = {"fan_mode": [None, []]}
        self._spec = {}
        self._luck = {}
        self._dbus_proxy = {}
        self._dependence = {}
        self._service_name = "org.asuslinux.Daemon"
        self._object_path = "/org/asuslinux/Supported"
        self._interface_name = "org.asuslinux.Daemon"

    def init(self):
        self._create_dbus("system", (
            ("spd", self._service_name, self._object_path,
             self._interface_name, None),))

        try:
            version = cmd("asusctl -v").split("\n")[0].strip()
        except Exception as e:
            version = ""
            self._log("Exception asusctl", e)

        self._log("Init Asusctl")
        self._log("    Version:", version)

        # TODO
        # supported = self._dbus_proxy["spd"].SupportedFunctions()
        # print("supported: ", supported)
        # supported: (False, (True,), (True, False, True),
        # (True, [], False, False), (True, False))

    # Private

    def _on_signal(self, proxy, sender_name, signal_name, parameters):
        if proxy and sender_name:
            self._log("Signal", signal_name + ":", tuple(parameters))
