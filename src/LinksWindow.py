# encoding: utf-8
#
# LinksWindow
#

import json

from gi.repository import Gtk


class LinksWindow(Gtk.Window):
    def __init__(self, *args, **kwargs):
        super().__init__(name="LinksWindow", *args, **kwargs)
        self._app = self.get_application()

        links = (
            ("https://asus-linux.org", "ASUS Linux", "Asus-Linux.org is an independent community effort that works to improve Linux support for Asus notebooks."),
            ("https://gitlab.com/asus-linux/asusctl/-/releases", "asusctl Releases", "asusctl Releases"),
            ("https://hwmon.wiki.kernel.org/", "HWMon Wiki", "This is the home of the Linux hwmon subsystem."),
            ("https://www.svgrepo.com", "SVG repo", "Search and find open source SVG graphics fastest way."),
            ("https://freesvg.org/", "Free SVG", "Free SVG vector files."),
            ("https://www.onlinewebfonts.com/icon/", "onlinewebfonts.com", "FREE SVG VECTOR ICONS"),
            ("https://cx-freeze.readthedocs.io/en/latest/distutils.html#cx-freeze-executable", "cx_Freeze", "cx_Freeze"),
            ("https://pyinstaller.readthedocs.io/en/stable/usage.html", "pyinstaller", "pyinstaller")
        )

        vbox = Gtk.VBox()

        grid = Gtk.Grid(column_homogeneous=False,
                        column_spacing=0,
                        row_spacing=0)

        context = grid.get_style_context()
        context.add_class("table")

        for top, item in enumerate(links):
            link = Gtk.LinkButton(uri=item[0], label=item[1])
            link.set_alignment(0.0, 0.0)
            b = Gtk.Box()
            b.add(link)
            link = b
            context = link.get_style_context()
            context.add_class("item")

            desc = Gtk.Label(label=item[2])
            desc.set_line_wrap(True)
            desc.set_justify(Gtk.Justification.LEFT)
            desc.set_alignment(0.0, 0.0)

            context = desc.get_style_context()
            context.add_class("item")

            if top == 0:
                context = link.get_style_context()
                context.add_class("top")
                context = desc.get_style_context()
                context.add_class("top")

            grid.attach(desc, 0, top, 1, 1)
            grid.attach(link, 1, top, 1, 1)
        scroll = Gtk.ScrolledWindow()
        scroll.add(grid)
        vbox.pack_start(scroll, True, True, 0)
        self.add(vbox)
        self.set_default_size(800, 450)
        self.show_all()

    @staticmethod
    def _read_json(path):
        with open(path, "r") as fp:
            data = json.load(fp)
            fp.close()
        return data

    def close(self, _widget=None, *_data) -> None:
        self.hide()
        self.destroy()

    def on_destroy(self, _widget=None, *_data) -> None:
        self.hide()
