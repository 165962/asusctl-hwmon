# encoding: utf-8
#

from gi.repository import Gio, Gtk, Gdk

from src.Emitter import Emitter
from src.TreeStore import TreeStore


class Tree:
    def __init__(self, application: Gtk.Application,
                 widget: Gtk.TreeView,
                 rows: list = None):
        self._app = application
        self._widget = widget
        self._tree_store = None
        if rows:
            self.init(rows)
        Emitter.on("change-lang", self.on_lang)

    def init(self, rows: list) -> None:
        self._tree_store = TreeStore(application=self._app)
        self._tree_store._treeView = self._widget
        self._tree_store.create(rows)
        self._tree_store.lang()
        self._widget.expand_all()

        # Get which item is selected
        selection = self._widget.get_selection()

        # When something new is selected, call _on_changed
        selection.connect('changed', self.on_selection_changed)

        menu = Gtk.Menu()
        menu_item = Gtk.MenuItem("A menu item")
        menu.append(menu_item)
        menu_item.show()

    def on_lang(self):
        self._tree_store.lang()

    def update(self):
        self._tree_store.update()

    def on_expand(self, _action: Gio.SimpleAction, _) -> None:
        self._widget.expand_all()

    def on_collapse(self, _action: Gio.SimpleAction, _) -> None:
        self._widget.collapse_all()

    @staticmethod
    def on_selection_changed(widget: Gtk.TreeView) -> None:
        widget.unselect_all()

    @staticmethod
    def cell_clicked(_widget, event):
        menu = Gtk.Menu()
        item = Gtk.MenuItem("1")
        menu.append(item)
        item = Gtk.MenuItem("2")
        menu.append(item)
        menu.show_all()
        Gtk.Menu.popup(menu, None, None, None, None, event.button,
                       event.time)
        if event.button == 1 and event.type == Gdk.EventType.BUTTON_PRESS:
            print("Double clicked on cell")
