# encoding: utf-8
#
# Emitter
#

class Emitter:
    _handlers = {}

    @staticmethod
    def on(name, handler):
        if name not in Emitter._handlers:
            Emitter._handlers[name] = []
        Emitter._handlers[name].append(handler)

    @staticmethod
    def off(name, handler):
        if name in Emitter._handlers:
            for i, call in enumerate(Emitter._handlers[name]):
                if call == handler:
                    Emitter._handlers[name].pop(i)

    @staticmethod
    def emit(name, *args):
        if name in Emitter._handlers:
            for listener in Emitter._handlers[name]:
                listener(*args)
