# encoding: utf-8
#
# MainWindow
#
# cli: ps, awk, cat, grep, nproc.
#

import os
from threading import Thread

from gi.repository import Gtk

import src.helper as h
from src import Tree, Scanner, Panel, cmd, Timer, Emitter, Notify
from src.views import Interval, Button


C_IND = 2
C_PATH = 7


@Gtk.Template(filename="./data/MainWindow.ui")
class MainWindow(Gtk.ApplicationWindow):
    __gtype_name__ = "MainWindow"
    _w_menu: Gtk.Menu = h.template("PrimaryMenu")
    _w_boxes: Gtk.Box = h.template("Boxes")
    _w_interval_label: Gtk.Label = h.template("IntervalLabel")
    _w_interval_combo: Gtk.ComboBoxText = h.template("IntervalCombo")
    _w_interval_value: Gtk.SpinButton = h.template("IntervalValue")
    _w_tree_view: Gtk.TreeView = h.template("TreeView")
    _w_copyleft: Gtk.Label = h.template("Copyleft")
    _w_label_cpu: Gtk.Label = h.template("LabelCPU")
    _w_label_pss: Gtk.Label = h.template("LabelPSS")
    _w_label_rss: Gtk.Label = h.template("LabelRSS")
    _w_header_bar: Gtk.HeaderBar = h.template("HeaderBar")
    _w_status_bar: Gtk.Statusbar = h.template("StatusBar")

    def __getattr__(self, name):
        if name[:10] == "on_notify_":
            if hasattr(Notify, name[10:]):
                def wrapper(*args):
                    call = getattr(Notify, name[10:])
                    self._app.set_notify(*call(args[0]))
            elif name[10:] == "screen_brightness":
                def wrapper(*args):
                    n = Notify.screen_brightness(args[0])
                    self._app.set_notify(*n)
            else:
                def wrapper(*args, **kwargs):
                    print("'%s' was called" % name, args, kwargs)
        else:
            def wrapper(*args, **kwargs):
                print("'%s' was called" % name, args, kwargs)
        return wrapper

    def __init__(self, *args, **kwargs):
        super().__init__(name="MainWindow", *args, **kwargs)

        """ Scanner """
        scanner = Scanner()
        scanner.scan()
        rows = scanner.get_rows()

        self._pid: int = os.getpid()
        self._app: Gtk.Application = self.get_application()
        self._tree: Tree = Tree(application=self._app,
                                widget=self._w_tree_view, rows=rows)
        self._panel: Panel = Panel(self._app, self)
        self._timer: [int, None] = None
        self._delay: int = self._app.config.delay
        self._pause: bool = False
        self._interval: Interval = Interval(combo=self._w_interval_combo,
                                            spin=self._w_interval_value)

        self._find_fan_to_rows(rows)

        self._init_theme()
        self._init_title()
        self._panel.init()

        self._create_app_action("tree-expand", self._tree.on_expand)
        self._create_app_action("tree-collapse", self._tree.on_collapse)

        self._w_label_pss.set_xalign(Gtk.Justification.LEFT)
        self._w_label_rss.set_xalign(Gtk.Justification.LEFT)

        self._w_boxes.pack_start(
            self._panel.get_view(0).widget, False, False, 0)

        self.set_icon_from_file("./data/icons/life-line.svg")

        # TODO
        image = Gtk.Image(stock=Gtk.STOCK_MEDIA_STOP)
        self._button_pause = Button(image=image)
        self._button_pause.on_clicked = self._on_loop_toggle
        self._w_header_bar.pack_end(self._button_pause.widget)

        self.lang()

        """ Interval """
        self._interval.on_change_mode = self._app.set_interval_mode
        self._interval.on_change_delay = self._app.set_delay
        self._interval.set_mode(self._app.config.interval_mode)
        self._interval.set_delay(self._app.config.delay)
        Emitter.on("change-interval-mode", self._interval.set_mode)
        Emitter.on("change-interval-delay", self._interval.set_delay)

        Emitter.on("change-lang", self.on_change_lang)
        Emitter.on("change-hide-app-name", self.on_change_hide_app_name)
        Emitter.on("change-interval-delay", self.on_change_delay)

        Emitter.on("update-k10temp-tctl", self.on_update_k10temp_tctl)
        Emitter.on("update-amdgpu-edge", self.on_update_amdgpu_edge)
        Emitter.on("update-nvme-composite",
                   self.on_update_nvme_composite)
        Emitter.on("update-iwlwifi_1-temp", self.on_update_iwlwifi_temp)

        Emitter.on("loop", self.on_update_tree)
        Emitter.on("loop", self.on_update_panel)
        Emitter.on("loop", self.on_update_nvidia)
        Emitter.on("loop", self.on_update_cpu)
        Emitter.on("loop", self.on_update_ram)

        self.show_all()
        self._loop()

    def _clear_timer(self):
        if self._timer:
            Timer.clear_timeout(self._timer)
            self._timer = None

    def _loop(self) -> None:
        self._clear_timer()
        if self._delay and not self._pause:
            Emitter.emit("loop")
            self._timer = Timer.set_timeout(self._delay, self._loop)

    @staticmethod
    def _thread(callback: callable):
        thread = Thread(target=callback)
        thread.start()
        thread.join()

    def _init_theme(self) -> None:
        self._on_theme_name_changed()
        Gtk.Settings().get_default().connect("notify::gtk-theme-name",
                                             self._on_theme_name_changed)

    def _init_title(self) -> None:
        path = "/sys/devices/virtual/dmi/id"
        with open(path + "/product_family", "r") as f:
            product_family = f.read().strip()
        with open(path + "/board_name", "r") as f:
            board_name = f.read().strip()
        if self._app.config.hide_app_name:
            self._w_header_bar.set_subtitle(None)
        else:
            self._w_header_bar.set_subtitle(self._app.application_name)

        self._w_header_bar.set_title(product_family + " " + board_name)

    def _find_fan_to_rows(self, rows):

        # TODO
        for row in rows:
            if row[C_IND] == "asus-cpu_fan":
                uri = "/device/driver/asus-nb-wmi/uevent"
                uevent = "DRIVER=asus-nb-wmi\n" \
                         "MODALIAS=platform:asus-nb-wmi\n"
                try:
                    path = os.path.dirname(row[C_PATH])
                    with open(path + uri, "r") as fp:
                        if fp.read() == uevent:
                            self._panel.set_fan(path)

                            # TODO
                            Emitter.on("update-fan", self.on_update_fan)

                        fp.close()
                except Exception as e:
                    print("Exception", e)

    def _create_app_action(self, name: str, callback: callable) -> None:
        action = h.gio_simple_action_new(name)
        action.connect("activate", callback)
        self.add_action(action)

    def _on_loop_toggle(self, _widget):
        self._pause = not self._pause
        if self._pause:
            image = Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY)
        else:
            image = Gtk.Image(stock=Gtk.STOCK_MEDIA_STOP)
            self._loop()
        self._button_pause.set_image(image=image)

    def _on_theme_name_changed(self, *_args):
        context = self.get_style_context()
        context.remove_class("MainWindow")
        # bg = context.get_background_color(Gtk.StateFlags.NORMAL)
        # rgba = [bg.red * 255, bg.green * 255, bg.blue * 255, bg.alpha]
        # rgba = [int(rgba[0]), int(rgba[1]), int(rgba[2]), rgba[3]]
        # y = 0.2126 * rgba[0] + 0.7152 * rgba[1] + 0.0722 * rgba[2]
        # is_dark = False
        # if y < 128:
        #     is_dark = True
        # srgba = [str(rgba[0]), str(rgba[1]), str(rgba[2]), "0.9"]
        # color = "rgba(" + ",".join(srgba) + ")"
        # if is_dark:
        #     text_shadow = "rgba(0,0,0,1)"
        # else:
        #     text_shadow = "rgba(255,255,255,1)"
        # css0 = '.MainWindow {\n' \
        #        '    background-color: ' + color + ';/* */\n' + \
        #        '    text-shadow: 0 0 1em ' + text_shadow + ';\n' + \
        #        '}'
        # css0 = '.MainWindow {\n' \
        #        '    background-color: ' + color + ';/* */\n' + \
        #        '}'
        css0 = ""
        with open("./data/style.css", 'r') as fp:
            css1 = fp.read()
        css = css0 + css1

        context.add_class("MainWindow")

        h.gtk_css_provider_load_from_data(bytes(css, encoding="utf-8"))

    def _on_update_ram(self):
        command = "cat /proc/{pid}/smaps".format(pid=self._pid)
        command += " | grep -i pss | awk '{Total+=$2} END {print Total}'"
        pss = cmd(command)
        self._w_label_pss.set_label(
            "pss: {0:.2f} MiB".format(int(pss) / 1024))

        command = "cat /proc/{pid}/smaps".format(pid=self._pid)
        command += " | grep -i rss | awk '{Total+=$2} END {print Total}'"
        rss = cmd(command)
        self._w_label_rss.set_label(
            "rss: {0:.2f} MiB".format(int(rss) / 1024))

    def lang(self):
        c = self._app.lang("🄯 2021 BAS with ABSOLUTELY NO WARRANTY.")

        """ Menu """
        for item in self._w_menu:
            item.set_label(self._app.lang(
                self._app.data_menu[item.get_action_name()]))

        """ Panel """
        self._panel.lang()

        """ Interval """
        self._w_interval_label.set_label(self._app.lang('Interval'))
        self._interval.set_items({0: self._app.lang("ms."),
                                  1: self._app.lang("sec.")})

        """ Copyleft """
        self._w_copyleft.set_label(c)

    def on_change_lang(self):
        self.lang()

    def on_change_delay(self, value: int):
        if value > 0:
            if self._delay == 0:
                self._delay = value
                self._loop()
            self._delay = value
        else:
            self._clear_timer()
            self._delay = 0

    def on_update_cpu(self):

        # TODO
        # command = "ps -eo %mem | awk -v c=$(nproc --all) '{s += $0} " \
        #           "END {print s / c}'"
        # command = "ps aux | awk -v c=$(nproc --all) '{s += $3} " \
        #           "END {print s / c}'"
        command = "ps -o pid,ppid,%mem | grep \"{pid} \""
        command = command.format(pid=self._pid)
        command += " | awk -v c=$(nproc --all) '{s+=$3} END {print s/c}'"
        cpu = cmd(command)
        self._w_label_cpu.set_label(
            "cpu: {cpu:.2f}%".format(cpu=float(cpu)))

    def on_update_ram(self):
        self._thread(self._on_update_ram)

    def on_update_fan(self, rpm):
        self._thread(lambda: self._panel.on_fan(rpm))

    def on_update_tree(self):
        self._thread(self._tree.update)

    def on_update_panel(self):
        self._thread(self._panel.check)

    def on_update_k10temp_tctl(self, value):
        self._thread(lambda: self._panel.set_view(93,
                                                  "{0:.1f}{1}".format(
                                                      float(value[0]),
                                                      value[1])))

    def on_update_amdgpu_edge(self, value):
        self._thread(lambda: self._panel.set_view(95,
                                                  "{0:.1f}{1}".format(
                                                      float(value[0]),
                                                      value[1])))

    def on_update_nvme_composite(self, value):
        self._thread(lambda: self._panel.set_view(97,
                                                  "{0:.1f}{1}".format(
                                                      float(value[0]),
                                                      value[1])))

    def on_update_iwlwifi_temp(self, value):
        if value[0] == "-":
            value = "-"
        else:
            value = "{0:.1f}{1}".format(float(value[0]), value[1])
        self._thread(lambda: self._panel.set_view(99, value))

    def on_update_nvidia(self):
        self._thread(self._panel.on_update_nvidia)

    def on_change_hide_app_name(self, value):
        if value:
            self._w_header_bar.set_subtitle(None)
        else:
            self._w_header_bar.set_subtitle(self._app.application_name)

    def on_destroy(self, _widget=None, *_data) -> None:
        self.hide()

    def destroy(self):
        Emitter.off("change-lang", self.on_change_lang)
        Emitter.off("update-fan", self.on_update_fan)
        Emitter.off("change-hide-app-name", self.on_change_hide_app_name)
        Emitter.off("change-interval-delay", self.on_change_delay)

        Emitter.off("loop", self.on_update_nvidia)
        Emitter.off("loop", self.on_update_tree)
        Emitter.off("loop", self.on_update_cpu)
        Emitter.off("loop", self.on_update_ram)

        super().destroy()
