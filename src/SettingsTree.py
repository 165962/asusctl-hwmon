# encoding: utf-8
#

import json

from gi.repository import Gtk, GObject, Pango

C_ID = 0
C_PARENT = 1
C_TYPE = 2
C_SKIP = 3
C_PACK = 4
C_VALUE = 5
C_TITLE = 6
C_KEY = 7
C_CLASS = 8


class SettingsTree(object):
    def __init__(self, lang_func):
        super().__init__()
        self._lang_func = lang_func

        path_view = "./data/config/panel_rows.json"

        self._config = self._read_json(path_view)
        self._head_labels = []
        self._body_labels = []

        # key, skip, icon, level, class, title, label, action, handler
        self._head = ["ID", "PARENT", "TYPE", "SKIP", "PACK", "VALUE", "TITLE", "KEY", "CLASS"]
        self._break_head = len(self._head) - 2

        self._store = Gtk.TreeStore()
        self._tree = Gtk.TreeView()

        self._create_head()
        self._create_body()
        self._tree.set_model(self._store)

    @staticmethod
    def _read_json(path):
        with open(path, "r") as fp:
            data = json.load(fp)
            fp.close()
        return data

    def _create_head(self):
        normal = Gtk.CellRendererText(weight=Pango.Weight.NORMAL)
        normal1 = Gtk.CellRendererText(weight=Pango.Weight.NORMAL)
        normal1.set_property("xalign", Pango.Alignment.CENTER)
        column_types = []

        for i, title in enumerate(self._head):
            cell = normal
            column_types.append(GObject.TYPE_STRING)
            column = Gtk.TreeViewColumn()
            column.set_title(title)
            column.pack_start(cell, True)
            column.add_attribute(cell, "text", i)
            self._tree.insert_column(column, i)

        self._store.set_column_types(column_types)
        self._lang_head()

    def _create_body(self):
        parent_iter = None
        for item in self._config:
            if item[C_ID] < 1:
                continue
            ids = []
            row = []
            for i, value in enumerate(item):
                ids.append(i)
                row.append(str(value))
                if i > self._break_head:
                    break
            child_iter = self._store.append(parent_iter)
            self._store.set(child_iter, ids, row)
        self._lang_body()

    def _lang_head(self):
        for i, key in enumerate(self._head):
            title = key.upper()
            if key == "N":
                title = "№"
            if key == "skip":
                title = "SHOW"
            title = self._lang_func(title)
            column = self._tree.get_column(i)
            column.set_title(title)

    def model_update_foreach(self, tree_store, tree_path, tree_iter):
        if tree_path:
            cid = int(tree_store.get_value(tree_iter, 0))
            row = list(filter(lambda x: x[C_ID] == cid, self._config))[0]
            for i, value in enumerate(row):
                value = self._lang_func(str(value))
                tree_store.set_value(tree_iter, i, value)
                if i > self._break_head:
                    break

    def _lang_body(self):
        self._store.foreach(self.model_update_foreach)

    def lang(self):
        self._lang_head()
        self._lang_body()

    def get_widget(self):
        return self._tree
