# encoding: utf-8
#

import json
from gi.repository import Gtk


class SettingsTable(object):
    def __init__(self, lang_func):
        super().__init__()
        self._lang_func = lang_func

        path_view = "./data/config/actions-view.json"

        self._config = self._read_json(path_view)
        self._head_labels = []
        self._body_labels = []

        # key, skip, icon, level, class, title, label, action, handler
        self._head = ["N", "skip", "level", "key", "title", "label"]

        self._grid = Gtk.Grid(column_homogeneous=False, column_spacing=0, row_spacing=0)
        context = self._grid.get_style_context()
        context.add_class("table")

        self._create_head()
        self._create_body()

    @staticmethod
    def _read_json(path):
        with open(path, "r") as fp:
            data = json.load(fp)
            fp.close()
        return data

    def _create_head(self):
        self._head_labels = []
        for left, key in enumerate(self._head):
            widget = Gtk.Label()
            context = widget.get_style_context()
            context.add_class("item")
            context.add_class("top")
            if left == 0:
                context.add_class("left")
            self._grid.attach(widget, left, 0, 1, 1)
            self._head_labels.append(widget)
        self._lang_head()

    def _create_body(self):
        for i, item in enumerate(self._config):
            top = i + 1
            self._body_labels.append([])
            for left, key in enumerate(self._head):
                widget = Gtk.Label(xalign=Gtk.Justification.LEFT)
                context = widget.get_style_context()
                context.add_class("item")
                if left == 0:
                    context.add_class("left")
                self._grid.attach(widget, left, top, 1, 1)
                self._body_labels[i].append(widget)
        self._lang_body()

    def _lang_head(self):
        for left, key in enumerate(self._head):
            label = key.upper()
            if key == "N":
                label = "№"
            if key == "skip":
                label = "SHOW"
            widget = self._head_labels[left]
            widget.set_label(label)

    def _lang_body(self):
        for i, item in enumerate(self._config):
            top = i + 1
            for left, key in enumerate(self._head):
                if key == "N":
                    widget = self._body_labels[i][left]
                    widget.set_label(str(top))
                    continue
                label = ""
                if key in item:
                    label = item[key]
                if key in ["skip"]:
                    label = not label
                elif key in ["level"] and label == "":
                    label = 1
                elif key in ["title", "label"]:
                    label = self._lang_func(label)
                widget = self._body_labels[i][left]
                widget.set_label(str(label))

    def lang(self):
        self._lang_head()
        self._lang_body()

    def get_widget(self):
        return self._grid
