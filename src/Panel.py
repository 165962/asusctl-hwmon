# encoding: utf-8
#
# MainWindow
#

from gi.repository import Gtk

from src.DialogCreateProfile import DialogCreateProfile
from src.DialogProfileFanCurve import DialogProfileFanCurve
from src.DialogRemoveProfile import DialogRemoveProfile
from src.models.Nvidia import Nvidia
from src.views import HBox, VBox, Label, Button, ActionCombo, \
    ActionScale, ActionSwitch

C_ID = 0
C_PARENT = 1
C_TYPE = 2
C_SKIP = 3
C_PACK = 4
C_VALUE = 5
C_TITLE = 6
C_KEY = 7
C_CLASS = 8
C_DEP = 9


class Panel:
    def __init__(self, app, parent):
        self._app = app
        self._parent = parent
        self._rows = self._app.read_json(app.config.panel_rows_file)
        self._conf = self._app.read_json(app.config.actions_conf_file)
        self._views = {}
        self._models: dict = {}
        self._nvidia = Nvidia()

        self._fan = ""
        self._check = []

        self._add_view(0, VBox())

    def init(self) -> None:
        #     self._init_model(view, conf)
        #     self._init_view(view, conf)

        for row in self._rows:
            if row[C_SKIP]:
                continue
            if row[C_TYPE] == "action":
                self.action(row)

        self._kbd_priority()

        for row in self._rows:
            if row[C_SKIP]:
                continue

            if row[C_DEP] > 0:
                if self.row_to_id(row[C_DEP])[C_SKIP]:
                    continue

            if row[C_ID] > 0 and self.init_row(row):
                view = self.get_view(row[C_ID])
                box = self.get_view(row[C_PARENT])
                if row[C_CLASS] == "ActionSwitch":
                    c_box = VBox()
                    c_box.pack_start(view, True, False, 0)
                    view = c_box
                if row[C_PACK] == "start":
                    box.pack_start(view, False, False, 0)
                elif row[C_PACK] == "end":
                    box.pack_end(view, False, False, 0)

    def init_row(self, row) -> bool:
        if row[C_TYPE] == "box":
            self.box(row)
        elif row[C_TYPE] == "icon":
            self.icon(row)
        elif row[C_TYPE] == "label":
            self.label(row)
        elif row[C_TYPE] == "info":
            self.info(row)
        elif row[C_TYPE] == "action":
            self.action(row)
        elif row[C_TYPE] == "button":
            mn = "asusctl_profile"
            self._init_model(row, self._conf[mn])
            if row[C_VALUE] == "asusctl_profile_next":
                if self.get_model(mn).is_method("next_profile"):
                    self._add_view(row[C_ID], self.btn_profile_next())
            elif row[C_VALUE] == "asusctl_profile_create":
                if self.get_model(mn).is_method("create_profile"):
                    self._add_view(row[C_ID], self.btn_profile_create())
            elif row[C_VALUE] == "asusctl_profile_remove":
                if self.get_model(mn).is_method("remove_profile"):
                    self._add_view(row[C_ID], self.btn_profile_remove())
            elif row[C_VALUE] == "asusctl_profile_fan_curve":
                if self.get_model(mn).is_method("get_profile_fan_curve"):
                    self._add_view(row[C_ID],
                                   self.btn_profile_fan_curve())

        # TODO
        return row[C_ID] in self._views

    def get_view(self, cid):
        return self._views[cid]

    def get_in_view(self, cid):
        if cid in self._views:
            return self._views[cid]

    def get_model(self, key):
        if key in self._models:
            return self._models[key]

    # TODO
    def set_fan(self, path):
        self._fan = path

    def set_view(self, cid, value):
        self.get_view(cid).set_value(value)

    def box(self, row):
        box = HBox()
        box.add_class("item")
        self._add_view(row[C_ID], box)

    def info(self, row):
        view = Label()
        view.add_class("info")
        self._add_view(row[C_ID], view)

    def icon(self, row):
        view = Label()
        view.add_class("icon")
        view.add_class("icon-" + row[C_VALUE])
        self._add_view(row[C_ID], view)

    def label(self, row):
        view = Label()
        view.set_value(row[C_VALUE])
        self._add_view(row[C_ID], view)

    def action(self, row):
        conf = self._conf[row[C_KEY]]
        self._init_model(row, conf)

        # TODO
        if row[C_SKIP]:
            return

        if not self.get_model(conf["model"]).is_method(
                conf["method_get"]):
            row[C_SKIP] = True
            return

        args = {
            "value": self._model_method(conf, "method_get"),
            "action": row[C_ID],
            "started": self._on_started,
            "finished": self._on_finished
        }

        if row[C_CLASS] == "ActionSwitch":
            view = ActionSwitch(**args)
        elif row[C_CLASS] == "ActionScale":
            args["min_value"] = self._model_method(conf, "method_min")
            args["max_value"] = self._model_method(conf, "method_max")
            view = ActionScale(**args)
        else:  # row[C_CLASS] == "ActionCombo":
            args["items"] = {}
            for key, item in self._model_method(conf,
                                                "method_list").items():
                args["items"][key] = self._app.lang(item)
            view = ActionCombo(**args)

        self._add_view(row[C_ID], view)

        view.key = row[C_KEY]

        # TODO
        if row[C_CLASS] == "ActionScale":
            view = self.get_view(row[C_ID])
            view.widget.set_size_request(200, -1)

        if "event" in conf:
            model = self._models[conf["model"]]

            # TODO
            def callback(value):
                self.set_view(row[C_ID], value)
                model.set_check(conf["event"], value)

            model.add_handler(conf["event"], callback)
            if "handler" in row:
                callback = getattr(self, row["handler"])
                model.add_handler(conf["event"], callback)

    def row_to_id(self, cid):
        return list(filter(lambda x: x[C_ID] == cid, self._rows))[0]

    def lang(self):
        for cid, item in self._views.items():
            if cid < 1:
                continue
            row = self.row_to_id(cid)
            c_type = row[C_TYPE]
            if c_type == "label":
                label = self._app.lang(row[C_VALUE])
                self.set_view(cid, label)
            elif c_type == "action" and row[C_CLASS] == "ActionCombo":
                conf = self._conf[row[C_VALUE]]
                items = {}
                for key, label in self._model_method(
                        conf, "method_list").items():
                    items[key] = self._app.lang(label)
                self.get_view(cid).set_items(items)

        # TODO
        view = self.get_in_view(23)
        if view:
            view.set_value(self._app.lang("Curve"))

    def check(self):
        for check in self._check:
            model = self.get_model(check[0])
            value = getattr(model, check[1])()
            old_value = model.get_check(check[2])
            if old_value != value:
                model.set_check(check[2], value)
                model.run_handlers(check[2], value)

                # # TODO
                # print("loop check", check[0], check[2], check[1],
                #       old_value, "->", value)

    def btn_profile_next(self):
        image = Gtk.Image(stock=Gtk.STOCK_GO_FORWARD)
        view = Button(image=image)
        view.on_clicked = lambda _value: \
            self.get_model("asusctl_profile").next_profile()
        return view

    def btn_profile_create(self):
        image = Gtk.Image(stock=Gtk.STOCK_ADD)
        view = Button(image=image)
        view.on_clicked = lambda _value: DialogCreateProfile(
            self._parent, self.get_model("asusctl_profile"),
            self.get_view(9)).dialog_run()
        return view

    def btn_profile_remove(self):
        image = Gtk.Image(stock=Gtk.STOCK_REMOVE)
        view = Button(image=image)
        view.on_clicked = self.on_click_remove_profile
        return view

    def btn_profile_fan_curve(self):
        view = Button(value="Curve")
        view.on_clicked = self.on_click_profile_fan_curve
        return view

    def on_fan(self, value):
        self.set_view(5, " {value} {rpm} "
                      .format(value=value, rpm=self._app.lang("RPM")))

    def on_remove_profile(self):
        combo = self.get_view(9)
        model = self.get_model("asusctl_profile")
        model.remove_profile(model.get_profile())
        combo.set_items(model.list_profile())
        combo.set_state(model.get_profile())

    def on_update_nvidia(self):
        f = "{edge}°C, GPU: {ugpu}, MEM: {umem}, {power}"
        if self._nvidia.is_method("gpu_data"):
            self.set_view(90, f.format(**self._nvidia.gpu_data()))

    def on_click_remove_profile(self, _value):
        dialog = DialogRemoveProfile(self._parent)
        if dialog.run() == Gtk.ResponseType.OK:
            self.on_remove_profile()
        dialog.destroy()

    def on_click_profile_fan_curve(self, _value):
        model = self.get_model("asusctl_profile")
        dialog = DialogProfileFanCurve(self._app, self._parent, model)
        dialog.run()
        # if dialog.run() == Gtk.ResponseType.OK:
        dialog.destroy()

    # Private

    def _add_view(self, cid, view):
        self._views[cid] = view

    @staticmethod
    def _import(namespace, class_name):
        return getattr(getattr(getattr(__import__(
            "src." + namespace + "." + class_name
        ), namespace), class_name), class_name)

    def _model_method(self, conf: dict, method_name: str):
        model = self._models[conf["model"]]
        if method_name in conf and model.is_method(conf[method_name]):
            return getattr(model, conf[method_name])()
        else:
            return None

    def _init_model(self, row, conf):

        # TODO
        if conf["class"] == "Fan":
            if self._fan == "":
                row[C_SKIP] = True
                return

        if not conf["model"] in self._models:
            model = self._import("models", conf["class"])
            if conf["class"] == "Fan":
                model = model(self._fan)
            else:
                model = model()

            # TODO
            if hasattr(model, "init"):
                getattr(model, "init")()

            self._models[conf["model"]] = model
            del model
        if "check" in conf and conf["check"]:
            model = self._models[conf["model"]]
            if model.is_method(conf["method_get"]):
                model.set_check(conf["event"],
                                getattr(model, conf["method_get"])())
                self._check.append(
                    (conf["model"], conf["method_get"], conf["event"]))

    def _on_started(self, action: str, value: int) -> None:
        view = self.get_view(action)
        conf = self._conf[view.key]
        model = self._models[conf["model"]]
        method = conf["method_set"]
        result = getattr(model, method)(value)
        # TODO if result and "command_set_" == method[:12]:
        if result and "command_set_" == method[:12]:
            view.subprocess_command(result)

    def _on_finished(self, action: str):
        view = self.get_view(action)
        conf = self._conf[view.key]
        model = self._models[conf["model"]]
        method = conf["method_get"]
        return getattr(model, method)()

    def _kbd_priority(self):
        kbd_data = (79, 107, 83, 87)
        kbd_length = len(kbd_data)
        for i, cid in enumerate(kbd_data):
            if not self.row_to_id(cid)[C_SKIP]:
                if i < kbd_length:
                    for j in range(i + 1, kbd_length):
                        self.row_to_id(kbd_data[j])[C_SKIP] = True
                    break
