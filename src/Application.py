# encoding: utf-8
#
# Application
#
# with ABSOLUTELY NO WARRANTY
#

import gettext
import json
import locale

from gi.repository import GLib, Gio, Gtk, GdkPixbuf, Notify

from src import Config, Emitter
from src.LinksWindow import LinksWindow
from src.MainWindow import MainWindow
from src.SettingsWindow import SettingsWindow
from src.helper import gio_simple_action_new


class Application(Gtk.Application):
    _lang: gettext.NullTranslations = None
    # Settings, Links.
    _windows: dict = {}
    application_id: str = None
    application_name: str = ""

    def __init__(self, application_name, **properties):
        super().__init__(application_id=self.application_id,
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
                         **properties)

        GLib.set_prgname(application_name)
        GLib.set_application_name(application_name)
        # GLib.get_user_special_dir(GLib.USER_DIRECTORY_PICTURES)
        # '/home/user/Pictures'
        # GLib.get_user_special_dir(GLib.USER_DIRECTORY_DOCUMENTS)
        # '/home/user/Documenten'
        # GLib.get_user_special_dir(GLib.USER_DIRECTORY_DOWNLOAD)
        # '/home/user/Downloads'

        self.application_id = "BAS.app.asusctl-HWMon"
        self.application_name = GLib.get_application_name()

        config_file = GLib.get_user_config_dir() + \
                      "/" + self.application_id + "/config.json"
        self.config = Config(config_file)
        self.data_menu = {
            "app.settings": "Settings",
            "app.links": "Links",
            "app.about": "About",
            "app.quit": "Quit"
        }

        """ Locale """
        # Default system values
        _locale, _encoding = locale.getdefaultlocale()
        self.locales = {"en_US": "English", "ru_RU": "Русский"}
        if self.config.locale in self.locales:
            lc = self.config.locale
        elif _locale in self.locales:
            lc = _locale
        else:
            lc = "en_US"
        self._lang_by_lc(lc)

        # TODO
        self._notify = None

        self.set_application_id(self.application_id)
        self.add_main_option("test", ord("t"), GLib.OptionFlags.NONE,
                             GLib.OptionArg.NONE, "Command line test",
                             None)

        Notify.init(self.application_id)

        Emitter.on("config-saved", self.on_config_saved)

    def _lang_by_lc(self, lc):
        lc_dir = self.config.locale_dir
        self._lang = gettext.translation("myapp", lc_dir, [lc])

    def _window_open(self, win_id) -> None:
        if win_id not in self._windows:
            if win_id == "links":
                self._windows[win_id] = LinksWindow(application=self)
            elif win_id == "settings":
                self._windows[win_id] = SettingsWindow(application=self)
            self._windows[win_id].win_id = win_id
            self._windows[win_id].connect("destroy", self._window_close)
        self._windows[win_id].present()

    def _window_close(self, window) -> None:
        if window.win_id in self._windows:
            self._windows[window.win_id].destroy()
            self._windows[window.win_id] = None
            self._windows.pop(window.win_id, None)

    def do_startup(self) -> None:
        Gtk.Application.do_startup(self)

        action = gio_simple_action_new("links", None)
        action.connect("activate", self.on_links)
        self.add_action(action)

        action = gio_simple_action_new("settings", None)
        action.connect("activate", self.on_settings)
        self.add_action(action)

        action = gio_simple_action_new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        action = gio_simple_action_new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

        # self.set_notify("Hello", "")

    def do_activate(self) -> None:
        # We only allow a single window and raise any existing ones
        if "main" not in self._windows:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self._windows["main"] = MainWindow(application=self)
        self._windows["main"].present()
        # Always top
        # self._windows["main"].set_keep_above(True)

    def do_shutdown(self):
        Gtk.Application.do_shutdown(self)

    def do_command_line(self, command_line) -> int:
        options = command_line.get_options_dict()
        # convert GVariantDict -> GVariant -> dict
        options = options.end().unpack()
        if "test" in options:
            # This is printed on the main instance, ./main.py --test 1.
            print("Test argument recieved: %s" % options["test"])
        self.activate()
        return 0

    @staticmethod
    def read_json(path):
        with open(path, "r") as fp:
            data = json.load(fp)
            fp.close()
        return data

    def lang(self, text):
        if text:
            text = self._lang.gettext(text)
        return text

    def set_lang(self, lang_id):
        i = 0
        for lc, lang in self.locales.items():
            if i == lang_id:
                self._lang_by_lc(lc)

                # TODO
                self.config.locale = lc

                Emitter.emit("change-lang")
            i = i + 1

    def set_delay(self, value):
        self.config.delay = value
        Emitter.emit("change-interval-delay", value)

    def set_notify(self, title=None, body=None, _icon=None) -> None:
        if not self._notify:
            # Create a new notification
            self._notify = Notify.Notification.new(title, body)
        else:
            # Update the title / body
            self._notify.update(title, body)

        # Show it
        self._notify.show()

    def set_hide_app_name(self, value):
        self.config.hide_app_name = value
        Emitter.emit("change-hide-app-name", value)

    def set_interval_mode(self, value):
        self.config.interval_mode = value
        Emitter.emit("change-interval-mode", value)

    def get_lang_id(self):
        i = 0
        for lc, lang in self.locales.items():
            if lc == self.config.locale:
                return i
            i = i + 1

    def get_languages(self):
        items = []
        for _lc, lang in self.locales.items():
            items.append(lang)
        return items

    def on_config_saved(self, path):
        self.set_notify(self.application_name,
                        "Configuration saved: " + path)

    def on_about(self, _action, _param) -> None:
        window = Gtk.AboutDialog(
            # logo_icon_name="./data/icons/life-line.svg",  #  pkg.name,
            program_name=self.application_name,
            version="0.0.1",  # pkg.version,
            comments="A {program_name} Application for GNOME".format(
                program_name=self.application_name
            ),
            # website='https://example/',
            copyright="Copyright 2021 BAS",
            license_type=Gtk.License.GPL_2_0,
            authors=["BAS"],
            # artists=["Name <mail@example>"],
            #  Translators: Replace "translator-credits" with your names,
            #  one name per line
            # translator_credits=self.lang('translator-credits'),
            wrap_license=True,
            modal=True,
            transient_for=self._windows["main"],
            use_header_bar=True,
        )
        pb = GdkPixbuf.Pixbuf.new_from_file("./data/icons/life-line.svg")
        window.set_logo(pb)
        window.connect("response", lambda _a, _b: window.close())
        window.present()

    def on_links(self, _action, _param) -> None:
        self._window_open(win_id="links")

    def on_settings(self, _action, _param) -> None:
        self._window_open(win_id="settings")

    def on_quit(self, _action, _param) -> None:
        self.quit()

    def destroy(self):
        Emitter.off("config-saved", self.on_config_saved)
        super(Application, self).destroy()
