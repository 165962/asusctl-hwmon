# encoding: utf-8
#
# import webbrowser
# webbrowser.open_new_tab('http://habrahabr.ru/')
#

from gi.repository import GLib, Gio, Gtk, Gdk


def template(name):
    return Gtk.Template.Child(name=name)


def gtk_builder_new_from_string(string: str, length: int) -> Gtk.Builder:
    return Gtk.Builder.new_from_string(string, length)


def gtk_css_provider_load_from_data(css):
    style_provider = Gtk.CssProvider()
    style_provider.load_from_data(css)
    Gtk.StyleContext.add_provider_for_screen(
        screen=Gdk.Screen.get_default(),
        provider=style_provider,
        priority=Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


def gio_dbus_node_info_new_for_xml(xml_data) -> Gio.DBusInterfaceInfo:
    return Gio.DBusNodeInfo.new_for_xml(xml_data)


def glib_variant_new_boolean(value: bool) -> GLib.Variant:
    return GLib.Variant.new_boolean(value)


def glib_variant_new_string(value: str) -> GLib.Variant:
    return GLib.Variant.new_string(value)


def gio_subprocess_new(command: list) -> Gio.Subprocess:
    flags = Gio.SubprocessFlags.STDOUT_SILENCE | \
            Gio.SubprocessFlags.STDERR_SILENCE
    return Gio.Subprocess.new(command, flags)


def gio_simple_action_new(name, parameter_type=None) -> Gio.SimpleAction:
    return Gio.SimpleAction.new(name, parameter_type)


def gio_simple_action_new_stateful(
        name: str, parameter_type,
        state: GLib.Variant) -> Gio.SimpleAction:
    return Gio.SimpleAction.new_stateful(name, parameter_type, state)
