# encoding: utf-8
#
# Scanner
#
# HWMon Wiki https://hwmon.wiki.kernel.org/
#

import os
import re

C_ID = 0
C_PID = 1
C_IND = 2
C_LEVEL = 3
C_NUMBER = 10
C_LENGTH = 4
C_LABEL = 5
C_INPUT = 6
C_PATH = 7
C_ROLE = 8
C_PROP = 9
C_TYPE = 11

path_hw_mon = "/sys/class/hwmon"
ignore_hw_mon = ["name", "device", "power", "subsystem", "uevent"]
regexp_hw_mon = "^(in|fan|pwm|temp|freq|curr|power)([0-9]+)_(.+)$"
regexp_hw_mon = re.compile(regexp_hw_mon,
                           flags=re.IGNORECASE | re.UNICODE)


class Scanner:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._rows = []

    def _scan_hw(self):
        data = []
        for item in self.scan_dir(path_hw_mon):
            hw = {
                "id": "{0}:{1}".format(item["device"], item["inode"]),
                "path": item["path"],
                "name": item["name"],
                "prop": "hwmon",
                "number": item["name"][5],
                "type": "",
                "label": self.read(item["path"] + "/name"),
                "input": "",
                "mons": {}
            }
            self._scan_mon(hw)
            data.append(hw)
        self._data_hw_mon(data)

    def _scan_mon(self, hw):
        for item in self.scan_dir(hw["path"]):
            if item["name"] in ignore_hw_mon:
                continue

            found = regexp_hw_mon.match(item["name"])
            if not found:
                continue

            ind = found[1] + found[2]
            mon = {
                "id": "{0}:{1}".format(item["device"], item["inode"]),
                "path": item["path"],
                "name": item["name"],
                "prop": found[1],
                "number": found[2],
                "type": found[3],
                "label": item["name"],
                "input": ""
            }

            if not (ind in hw["mons"]):
                hw["mons"][ind] = {}
            hw["mons"][ind][mon["type"]] = mon

    @staticmethod
    def create_child(children: list, data: dict):
        children.append({
            "id": data["id"],
            "ind": data["ind"],
            "path": data["path"],
            "type": data["type"],
            "prop": data["prop"],
            "number": data["number"],
            "label": data["label"],
            "input": data["input"],
            "value": data["input"],
            "unit": "",
            "role": data["role"],
            "children": []
        })

    def _data_hw_mon(self, data):
        children = []
        for item in data:
            c0 = len(children)
            md = item
            md["ind"] = md["label"].lower()
            md["role"] = ""
            self.create_child(children, md)

            for ind in item["mons"]:
                other = []
                label = ind
                mon_data = None
                label_data = None
                input_data = None

                for mon in item["mons"][ind]:
                    mon_data = item["mons"][ind][mon]

                    # TODO
                    if label == mon_data["prop"] + mon_data["number"]:
                        label = mon_data["prop"]

                    if "label" == mon_data["type"]:
                        label_data = mon_data
                        label_data["role"] = "label"
                        label = self.read(mon_data["path"])
                    elif "input" == mon_data["type"]:
                        input_data = mon_data
                        input_data["role"] = input_data["prop"]
                        input_data["input"] = self.read(mon_data["path"])
                    else:
                        other.append(item["mons"][ind][mon])

                if input_data:
                    md = input_data
                    md["label"] = label
                elif label_data:
                    md = label_data
                else:
                    md = {
                        "id": "",
                        "ind": "",
                        "path": "",
                        "prop": mon_data["prop"],
                        "number": mon_data["number"],
                        "type": "",
                        "label": label,
                        "input": "",
                        "role": ""
                    }

                md["ind"] = item["ind"] + "-" + md["label"].lower()
                self.create_child(children[c0]["children"], md)

                for mon in other:
                    c1 = len(children[c0]["children"]) - 1
                    mon["ind"] = ""
                    mon["label"] = mon["type"]
                    mon["role"] = mon["prop"]
                    mon["ind"] = md["ind"] + "-" + mon["label"].lower()

                    self.create_child(
                        children[c0]["children"][c1]["children"], mon)

        # TODO
        rows = []
        self._recursion(rows, "", 1, children)
        self._rows = rows
        # for row in rows:
        #     indent = (row[C_LEVEL] - 1) * "  "
        #     print(indent, row[C_ID], row[C_PID], row[C_LENGTH],
        #           row[C_LABEL])
        # exit(0)

    def _recursion(self, rows: list, pid, level, children):
        for child in children:
            length = len(child["children"])
            # id, ind, path, prop, number, type, label, input, role
            row = [
                child["id"],
                pid,
                child["ind"],
                level,
                length,
                child["label"],
                child["input"],
                child["path"],
                child["role"],

                child["prop"],
                child["number"],
                child["type"]
            ]

            s = True
            j = -1
            for i, r in enumerate(rows):
                if r[C_ID] == pid:
                    j = i
                    break
            if j > -1:
                parent = rows[j]
                if parent[C_ROLE] == "" and parent[C_LENGTH] == 1:
                    row[C_PID] = parent[C_PID]
                    row[C_LEVEL] = parent[C_LEVEL]
                    row[C_LABEL] = parent[C_LABEL] + " " + row[C_LABEL]
                    rows[j] = row
                    s = False
            if s:
                rows.append(row)

            if length:
                self._recursion(rows, child["id"], level + 1, child["children"])

    @staticmethod
    def scan_dir(path):
        data = []
        dir_entry = os.scandir(path)
        with dir_entry as it:
            for entry in it:
                # if not entry.name.startswith(".") and entry.is_file():
                if entry.is_file():
                    type_entry = "file"
                elif entry.is_dir():
                    type_entry = "dir"
                else:
                    type_entry = None

                stat = entry.stat()
                data.append({
                    "device": stat.st_dev,
                    "inode": stat.st_ino,
                    "type": type_entry,
                    "path": path + "/" + entry.name,
                    "name": entry.name
                })
        dir_entry.close()
        return data

    def scan(self):
        return self._scan_hw()

    @staticmethod
    def read(path):
        fp = None
        contents = ""
        try:
            with open(path, "r") as fp:
                contents = fp.read()
        except IOError as e:
            print("Except IOError", e.errno, e.strerror, path)
        finally:
            if fp:
                fp.close()
                if contents:
                    contents = contents.strip()
        return contents

    def get_rows(self):
        return self._rows
