# encoding: utf-8
#

from gi.repository import Gtk


class Box:
    def __init__(self, widget: Gtk.Box = None):
        self.widget = widget
        if widget is None:
            self.widget = Gtk.Box()

    def add_class(self, class_name):
        self.widget.get_style_context().add_class(class_name)

    def set_css_name(self, css_name):
        self.widget.set_css_name(css_name)

    def pack_start(self, child, expand, fill, padding):
        self.widget.pack_start(child.widget, expand, fill, padding)

    def pack_end(self, child, expand, fill, padding):
        self.widget.pack_end(child.widget, expand, fill, padding)