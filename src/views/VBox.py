# encoding: utf-8
#

from gi.repository import Gtk

from src.views.Box import Box


class VBox(Box):
    def __init__(self, widget: Gtk.VBox = None):
        self.widget = widget
        if widget is None:
            self.widget = Gtk.VBox()
        super().__init__(self.widget)
