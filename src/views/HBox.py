# encoding: utf-8
#

from gi.repository import Gtk

from src.views.Box import Box


class HBox(Box):
    def __init__(self, widget: Gtk.HBox = None):
        self.widget = widget
        if widget is None:
            self.widget = Gtk.HBox()
        super().__init__(self.widget)
