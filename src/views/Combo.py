# encoding: utf-8
#

from gi.repository import Gtk


class Combo:
    def __init__(self, items=None, value=None,
                 widget: Gtk.ComboBoxText = None):
        self._is_listen = True
        self.on_changed = None
        self.widget = widget
        if widget is None:
            self.widget = Gtk.ComboBoxText()
        self.widget.set_model(Gtk.ListStore(str, int))
        self.widget.set_entry_text_column(0)
        if items is not None:
            self.set_items(items)
        if value is not None:
            self.set_value(value)
        self.widget.connect("changed", self._on_changed)

    def _on_changed(self, _widget: Gtk.ComboBoxText) -> None:
        if self._is_listen and self.on_changed:
            self.on_changed(self.get_value())

    def on(self, handler: callable) -> None:
        self.on_changed = handler

    def set_items(self, items: dict) -> None:
        value = self.get_value()
        store = self.widget.get_model()
        for row in store:
            self._is_listen = False
            store.remove(row.iter)
            self._is_listen = True
        for k, v in items.items():
            self._is_listen = False
            store.append([v, int(k)])
            self._is_listen = True
        if value is not None:
            self.set_value(value)

    def set_value(self, value: int) -> None:
        if value != self.get_value():
            for row in self.widget.get_model():
                if value == row[1]:
                    self._is_listen = False
                    self.widget.set_active_iter(row.iter)
                    self._is_listen = True

    def get_value(self) -> [int, None]:
        value = None
        active_iter = self.widget.get_active_iter()
        if active_iter is not None:
            model = self.widget.get_model()
            value = model[active_iter][1]
        return value

    def destroy(self) -> None:
        self.widget.disconnect_by_func(self._on_changed)
        self.widget.destroy()
