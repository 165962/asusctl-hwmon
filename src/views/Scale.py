# encoding: utf-8
#

from gi.repository import Gtk


class Scale:
    def __init__(self, value=None, min_value=None, max_value=None,
                 widget: Gtk.Scale = None):
        self._is_listen = True
        self._indicator = None
        self.on_changed = None
        self.widget = widget
        if widget is None:
            self.widget = Gtk.Scale()
        if value is not None:
            self.set_value(value)
        else:
            value = 0
        orientation = Gtk.Orientation.HORIZONTAL
        adjustment = Gtk.Adjustment(value, min_value, max_value, 1, 0, 0)
        self.widget.set_adjustment(adjustment)
        self.widget.set_orientation(orientation)
        self.widget.set_digits(0)
        # self.widget.set_draw_value(False)
        self.widget.set_value_pos(Gtk.PositionType.LEFT)
        if max_value - min_value < 10:
            for i in range(min_value, max_value + 1):
                self.widget.add_mark(i, Gtk.PositionType.TOP, "")
        # self.widget.set_has_origin(True)

        self.widget.connect("value-changed", self._on_changed)

    def _on_changed(self, _w: Gtk.CheckButton, _b: bool = None) -> None:
        if self._is_listen and self.on_changed:
            self.on_changed(self.get_value())

    def on(self, handler: callable) -> None:
        self.on_changed = handler

    def set_value(self, value: int) -> None:
        if self._indicator:
            self._indicator.set_label(str(value))
        self._is_listen = False
        self.widget.set_value(value)
        self._is_listen = True

    def get_value(self) -> int:
        return int(self.widget.get_value())

    def destroy(self) -> None:
        self.widget.disconnect_by_func(self._on_changed)
        self.widget.destroy()
