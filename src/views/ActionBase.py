# encoding: utf-8
#

from gi.repository import Gio

from src.helper import gio_subprocess_new


class ActionBase:
    def __init__(self, action=None, started=None, finished=None):
        self._action = action
        self._started = started
        self._finished = finished
        self._is_listen = True

    def _on_changed(self, _widget, _value=None):
        if self._started and self._is_listen:
            self._started(self._action, self.get_value())

    def _subprocess_finished(self, subprocess: Gio.Subprocess,
                             result: Gio.AsyncResult) -> None:
        try:
            subprocess.wait_check_finish(result)
        except Exception as e:

            # TODO User click pkexec cancel
            if hasattr(e, "code") and 126 == getattr(e, "code"):
                print("cancel")
                pass
            else:
                print("Exception", e)
                raise

        state = self._finished(self._action)
        if state != self.get_value():
            self.set_state(state)
        self._wait = False

    def subprocess_command(self, command: list) -> None:
        subprocess = gio_subprocess_new(command)
        subprocess.wait_check_async(None, self._subprocess_finished)

    def set_state(self, value: int) -> None:
        self.set_value(value)
