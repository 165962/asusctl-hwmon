# encoding: utf-8
#

from gi.repository import Gtk


class Label:
    def __init__(self, label=None, widget: Gtk.Label = None):
        self.widget = widget
        if widget is None:
            self.widget = Gtk.Label()
        if label is not None:
            self.widget.set_label(label)

    def add_class(self, class_name):
        self.widget.get_style_context().add_class(class_name)

    def set_value(self, value):
        self.widget.set_label(value)

    def set_label(self, label):
        self.widget.set_label(label)

    def set_css_name(self, css_name):
        self.widget.set_css_name(css_name)