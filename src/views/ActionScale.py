# encoding: utf-8
#

from src.views.ActionBase import ActionBase
from src.views.Scale import Scale


class ActionScale(ActionBase, Scale):
    def __init__(self, action, value=None, min_value=None,
                 max_value=None, started=None, finished=None,
                 widget=None):
        Scale.__init__(self, value=value, min_value=min_value,
                       max_value=max_value, widget=widget)
        ActionBase.__init__(self, action=action, started=started,
                            finished=finished)
