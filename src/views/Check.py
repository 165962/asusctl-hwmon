# encoding: utf-8
#

from gi.repository import Gtk


class Check:
    def __init__(self, value=None, widget: Gtk.CheckButton = None):
        self._is_listen = True
        self.on_changed = None
        self.widget = widget
        if widget is None:
            self.widget = Gtk.CheckButton()
        if value is not None:
            self.set_value(value)
        self.widget.connect("toggled", self._on_changed)

    def _on_changed(self, _widget: Gtk.CheckButton) -> None:
        if self._is_listen and self.on_changed:
            self.on_changed(self.get_value())

    def on(self, handler: callable) -> None:
        self.on_changed = handler

    def set_value(self, value: bool) -> None:
        self._is_listen = False
        self.widget.set_active(value)
        self._is_listen = True

    def get_value(self) -> bool:
        return self.widget.get_active()

    def destroy(self) -> None:
        self.widget.disconnect_by_func(self._on_changed)
        self.widget.destroy()
