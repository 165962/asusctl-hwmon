# encoding: utf-8
#

from src.views.ActionBase import ActionBase
from src.views.Switch import Switch


class ActionSwitch(ActionBase, Switch):
    def __init__(self, action, value=None, started=None, finished=None,
                 widget=None):
        Switch.__init__(self, value=value, widget=widget)
        ActionBase.__init__(self, action=action, started=started,
                            finished=finished)
