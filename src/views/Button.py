# encoding: utf-8
#

from gi.repository import Gtk


class Button:
    def __init__(self, value=None, image: Gtk.Image = None,
                 widget: Gtk.Button = None):
        self._is_listen = True
        self.on_clicked = None
        self.widget = widget
        if widget is None:
            self.widget = Gtk.Button()
        if value is not None:
            self.set_value(value)
        if image is not None:
            self.set_image(image)
        self.widget.connect("clicked", self._on_clicked)

    def _on_clicked(self, _w: Gtk.Button) -> None:
        if self._is_listen and self.on_clicked:
            self.on_clicked(self.get_value())

    def on(self, handler: callable) -> None:
        self.on_clicked = handler

    def set_value(self, value: str) -> None:
        self._is_listen = False
        self.widget.set_label(value)
        self._is_listen = True

    def set_image(self, image: Gtk.Image) -> None:
        self.widget.set_image(image=image)

    def get_value(self) -> str:
        return self.widget.get_label()

    def destroy(self) -> None:
        self.widget.disconnect_by_func(self._on_changed)
        self.widget.destroy()
