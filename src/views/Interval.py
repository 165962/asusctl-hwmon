# encoding: utf-8
#

from decimal import Decimal, getcontext

from gi.repository import Gtk

from src.views.Combo import Combo
from src.views.Spin import Spin


class Interval:
    def __init__(self, delay: int = None, items: dict = None,
                 mode: int = None, combo: Gtk.ComboBoxText = None,
                 spin: Gtk.SpinButton = None):
        self._delay = 0
        self.on_change_mode = None
        self.on_change_delay = None
        self.spin = Spin(widget=spin)
        self.combo = Combo(widget=combo)
        if items is not None:
            self.set_items(items)
        if mode is not None:
            self.set_mode(mode)
        else:
            self._auto_format()
        if delay is not None:
            self.set_delay(delay)
        self.combo.on_changed = self._on_change_mode
        self.spin.on_changed = self._on_change_delay

    def _auto_format(self) -> None:
        mode = self.get_mode()
        if mode == 1:
            self.spin.set_digits(3)
            a = (self._delay / 1000, 0, 100, .100, 0, 0)
        else:
            self.spin.set_digits(0)
            a = (self._delay, 0, 100000, 100, 0, 0)
        self.spin.set_format(*a)

    def _on_change_mode(self, value):
        if self.on_change_mode:
            self.on_change_mode(value)

    def _on_change_delay(self, value: float) -> None:
        if self.on_change_delay:
            mode = self.get_mode()
            if mode == 0:
                delay = self.spin.get_value_as_int()
            elif mode == 1:
                delay = self.spin.get_value()
                if delay >= 0.001:
                    getcontext().prec = 3
                    delay = int(Decimal(delay) * Decimal(1000))
                else:
                    delay = 0
            else:
                delay = value
            self.on_change_delay(delay)

    def get_mode(self) -> [int, None]:
        return self.combo.get_value()

    def set_mode(self, value: int):
        self.combo.set_value(value)
        self._auto_format()

    def set_items(self, items: dict):
        self.combo.set_items(items)

    def set_delay(self, value: [int, float]):
        if self._delay != value:
            self._delay = value
            if self.get_mode() == 1:
                value = value / 1000
            self.spin.set_value(value)

    def destroy(self) -> None:
        self.spin.destroy()
        self.combo.destroy()
