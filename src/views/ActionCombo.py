# encoding: utf-8
#

from src.views.ActionBase import ActionBase
from src.views.Combo import Combo


class ActionCombo(ActionBase, Combo):
    def __init__(self, action, items=None, value=None, started=None,
                 finished=None, widget=None):
        Combo.__init__(self, items=items, value=value, widget=widget)
        ActionBase.__init__(self, action=action, started=started,
                            finished=finished)
