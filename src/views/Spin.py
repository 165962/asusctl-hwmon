# encoding: utf-8
#

from gi.repository import Gtk


class Spin:
    def __init__(self, value=None, widget: Gtk.SpinButton = None):
        self._is_listen = True
        self.on_changed = None
        self.widget = widget
        if widget is None:
            self.widget = Gtk.SpinButton()
        if value is not None:
            self.set_value(value)
        self.widget.connect("value-changed", self._on_changed)

    def _on_changed(self, _widget: Gtk.SpinButton) -> None:
        if self._is_listen and self.on_changed:
            self.on_changed(self.get_value())

    def on(self, handler: callable) -> None:
        self.on_changed = handler

    def set_digits(self, value: int) -> None:
        self._is_listen = False
        self.widget.set_digits(value)
        self._is_listen = True

    def set_format(self, *attr) -> None:
        self._is_listen = False
        self.widget.set_adjustment(Gtk.Adjustment(*attr))
        self._is_listen = True

    def set_value(self, value: float) -> None:
        self._is_listen = False
        self.widget.set_value(value)
        self._is_listen = True

    def get_value(self) -> float:
        return self.widget.get_value()

    def get_value_as_int(self) -> int:
        return self.widget.get_value_as_int()

    def destroy(self) -> None:
        self.widget.disconnect_by_func(self._on_changed)
        self.widget.destroy()
