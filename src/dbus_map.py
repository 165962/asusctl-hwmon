dbus_map = (
# DBUS NAME                       ,DBUS PATH                             ,DBUS INTERFACE                           ,DBUS PROPERTY          ,DBUS METHOD        ,SIGNATURES  ,MY METHOD                ,LUCK METHOD
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"ActiveProfileName","|s"        ,"active_name"            ,"_active_profile_name"    ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"ActiveName"       ,"|s"        ,"active_name"            ,"_active_name"            ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"Profile"          ,"|s"        ,"active_data"            ,"_profile"                ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"ActiveData"       ,"|(syybus)" ,"active_data"            ,"_active_data"            ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"ProfileNames"     ,"|as"       ,"profile_names"          ,"_profile_names"          ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"SetProfile"       ,"s|"        ,"set_profile"            ,"_set_profile"            ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"NextProfile"      ,"|"         ,"next_profile"           ,"_next_profile"           ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"Remove"           ,"s|"        ,"remove_profile"         ,"_remove_profile"         ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"NewOrModify"      ,"(syybus)|" ,"new_or_modify"          ,"_new_or_modify"          ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Profile"              ,"org.asuslinux.Daemon"                   ,""                     ,"Profiles"         ,"|a(syybus)","profiles"               ,"_profiles"               ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Charge"               ,"org.asuslinux.Daemon"                   ,""                     ,"Limit"            ,"|n"        ,"limit"                  ,"_limit"                  ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Charge"               ,"org.asuslinux.Daemon"                   ,""                     ,"SetLimit"         ,"y|"        ,"set_limit"              ,"_set_limit"              ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Led"                  ,"org.asuslinux.Daemon"                   ,"LedBrightness"        ,""                 ,"n"         ,"get_kbd_bright"         ,"_led_brightness"         ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Led"                  ,"org.asuslinux.Daemon"                   ,""                     ,"SetBrightness"    ,"u|"        ,"set_kbd_bright"         ,"_set_brightness"         ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Gfx"                  ,"org.asuslinux.Daemon"                   ,""                     ,"Power"            ,"|u"        ,"power"                  ,"_power"                  ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Gfx"                  ,"org.asuslinux.Daemon"                   ,""                     ,"Vendor"           ,"|u"        ,"vendor"                 ,"_vendor"                 ),
("org.asuslinux.Daemon"           ,"/org/asuslinux/Gfx"                  ,"org.asuslinux.Daemon"                   ,""                     ,"SetVendor"        ,"u|u"       ,"set_vendor"             ,"_set_vendor"             ),
("org.freedesktop.UPower"         ,"/org/freedesktop/UPower/KbdBacklight","org.freedesktop.UPower.KbdBacklight"    ,""                     ,"SetBrightness"    ,"i|"        ,"set_brightness"         ,"_set_brightness"         ),
("org.freedesktop.UPower"         ,"/org/freedesktop/UPower/KbdBacklight","org.freedesktop.UPower.KbdBacklight"    ,""                     ,"GetBrightness"    ,"|i"        ,"get_brightness"         ,"_get_brightness"         ),
("org.freedesktop.UPower"         ,"/org/freedesktop/UPower/KbdBacklight","org.freedesktop.UPower.KbdBacklight"    ,""                     ,"GetMaxBrightness" ,"|i"        ,"max_brightness"         ,"_max_brightness"         ),

("org.freedesktop.NetworkManager" ,"/org/freedesktop/NetworkManager"     ,"org.freedesktop.NetworkManager"         ,"Version"              ,""                 ,"s"         ,"get_version"            ,"_get_version"            ),
("org.freedesktop.NetworkManager" ,"/org/freedesktop/NetworkManager"     ,"org.freedesktop.NetworkManager"         ,"WirelessEnabled"      ,""                 ,"b"         ,"wireless_enabled"       ,"_wireless_enabled"       ),
("org.freedesktop.NetworkManager" ,"/org/freedesktop/NetworkManager"     ,"org.freedesktop.DBus.Properties"        ,""                     ,"Get"              ,"ss|v"      ,""                       ,""                        ),
("org.freedesktop.NetworkManager" ,"/org/freedesktop/NetworkManager"     ,"org.freedesktop.DBus.Properties"        ,""                     ,"Set"              ,"ssv|"      ,""                       ,""                        ),

("org.bluez"                      ,"/org/bluez/hci0"                     ,"org.bluez.Adapter1"                     ,"Powered"              ,""                 ,"b"         ,"powered"                ,"_powered"                ),
("org.bluez"                      ,"/org/bluez/hci0"                     ,"org.freedesktop.DBus.Properties"        ,""                     ,"Get"              ,"ss|v"      ,""                       ,""                        ),
("org.bluez"                      ,"/org/bluez/hci0"                     ,"org.freedesktop.DBus.Properties"        ,""                     ,"Set"              ,"ssv|"      ,""                       ,""                        ),

("org.gnome.SettingsDaemon.Power" ,"/org/gnome/SettingsDaemon/Power"     ,"org.gnome.SettingsDaemon.Power.Screen"  ,"Brightness"           ,""                 ,"i"         ,"brightness"             ,"_brightness"             ),
("org.gnome.SettingsDaemon.Power" ,"/org/gnome/SettingsDaemon/Power"     ,"org.gnome.SettingsDaemon.Power.Keyboard","Brightness"           ,""                 ,"i"         ,"brightness"             ,"_brightness"             ),
#("org.gnome.SettingsDaemon.Power" ,"/org/gnome/SettingsDaemon/Power"     ,"org.freedesktop.DBus.Properties"        ,""                     ,"Get"              ,"ss|v"      ,""                       ,""                        ),
#("org.gnome.SettingsDaemon.Power" ,"/org/gnome/SettingsDaemon/Power"     ,"org.freedesktop.DBus.Properties"        ,""                     ,"Set"              ,"ssv|"      ,""                       ,""                        ),

("org.gnome.SettingsDaemon.Rfkill","/org/gnome/SettingsDaemon/Rfkill"    ,"org.gnome.SettingsDaemon.Rfkill"        ,"AirplaneMode"         ,""                 ,"b"         ,"airplane_mode"          ,"_airplane_mode"          ),
("org.gnome.SettingsDaemon.Rfkill","/org/gnome/SettingsDaemon/Rfkill"    ,"org.gnome.SettingsDaemon.Rfkill"        ,"BluetoothAirplaneMode",""                 ,"b"         ,"bluetooth_airplane_mode","_bluetooth_airplane_mode"),
#("org.gnome.SettingsDaemon.Rfkill","/org/gnome/SettingsDaemon/Rfkill"    ,"org.freedesktop.DBus.Properties"        ,""                     ,"Get"              ,"ss|v"      ,""                       ,""                        ),
#("org.gnome.SettingsDaemon.Rfkill","/org/gnome/SettingsDaemon/Rfkill"    ,"org.freedesktop.DBus.Properties"        ,""                     ,"Set"              ,"ssv|"      ,""                       ,""                        ),

("org.kde.Solid.PowerManagement","/org/kde/Solid/PowerManagement/Actions/BrightnessControl","org.kde.Solid.PowerManagement.Actions.BrightnessControl","","setBrightness","i|","set_brightness","_set_brightness"),
("org.kde.Solid.PowerManagement","/org/kde/Solid/PowerManagement/Actions/BrightnessControl","org.kde.Solid.PowerManagement.Actions.BrightnessControl","","brightness","|i","get_brightness","_get_brightness"),
("org.kde.Solid.PowerManagement","/org/kde/Solid/PowerManagement/Actions/BrightnessControl","org.kde.Solid.PowerManagement.Actions.BrightnessControl","","brightnessMax","|i","max_brightness","_max_brightness"),
("org.kde.Solid.PowerManagement","/org/kde/Solid/PowerManagement/Actions/KeyboardBrightnessControl","org.kde.Solid.PowerManagement.Actions.KeyboardBrightnessControl","","setKeyboardBrightness","i|","set_brightness","_set_brightness"),
("org.kde.Solid.PowerManagement","/org/kde/Solid/PowerManagement/Actions/KeyboardBrightnessControl","org.kde.Solid.PowerManagement.Actions.KeyboardBrightnessControl","","keyboardBrightness","|i","get_brightness","_get_brightness"),
("org.kde.Solid.PowerManagement","/org/kde/Solid/PowerManagement/Actions/KeyboardBrightnessControl","org.kde.Solid.PowerManagement.Actions.KeyboardBrightnessControl","","keyboardBrightnessMax","|i","max_brightness","_max_brightness"),
)

# org.asuslinux.Daemon /org/asuslinux/Led
#     method: SetAwakeEnabled
#     method: SetSleepEnabled
#     method: SetLedMode
#     method: NextLedMode
#     method: PrevLedMode
#     property: AwakeEnabled
#     property: SleepEnabled
#     property: LedModes
#     property: LedMode