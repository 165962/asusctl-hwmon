import os
import subprocess


def cmd1(command: str) -> str:
    result = ""
    output = os.popen(command, "r").read()
    if isinstance(output, str):
        result = output.strip()
    return result


def cmd(command: str) -> str:
    result = ""
    call = subprocess.run(
        ["sh"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
        text=True, input=command)
    if isinstance(call.stdout, str):
        result = call.stdout.strip()
    return result

