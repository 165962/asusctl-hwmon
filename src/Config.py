# encoding: utf-8
#

import json
import os

from src.Emitter import Emitter
from src.Timer import Timer


class Config(object):
    __slots__ = ["_file", "_data", "_timer"]
    __dict__ = {
        "_file": "",
        "_timer": None,
        "_data": {
            "locale_dir": "./data/lang",
            "panel_rows_file": "./data/config/panel_rows.json",
            "actions_conf_file": "./data/config/actions-conf.json",
            "delay": 1000,
            "locale": "",
            "interval_mode": 0,
            "hide_app_name": 0
        }}

    def __init__(self, file):
        super().__init__()
        self._file = file
        self._timer = None
        # TODO
        config_dir = os.path.dirname(self._file)
        try:
            if not os.path.exists(config_dir):
                os.makedirs(config_dir)
        except FileExistsError:
            # directory already exists
            pass
        self.open()

    def __getattr__(self, name: str):
        # print(name)
        if name in self.__dict__["_data"]:
            return self._data[name]
        elif name in self.__dict__:
            return self.__dict__[name]
        else:
            raise AttributeError(name)

    def __setattr__(self, name: str, value):
        # print("set", name, value)
        if name in self.__dict__:
            self.__dict__[name] = value
        else:
            self.__dict__["_data"][name] = value
            self._clear_timer()
            self._timer = Timer.set_timeout(1000, self.save)

    def _clear_timer(self):
        if self._timer:
            Timer.clear_timeout(self._timer)
            self._timer = None

    def open(self):
        path = self._file
        if os.path.exists(path):
            with open(path, "r", encoding='utf-8') as f:
                data = json.load(f)
                # self.__dict__["_data"] = data
                for key, value in data.items():
                    self.__dict__["_data"][key] = value
                f.close()

    def save(self):
        self._clear_timer()
        f = open(self._file, "w", encoding="utf-8")
        json.dump(self._data, f, ensure_ascii=False, indent=4)
        f.close()
        Emitter.emit("config-saved", self._file)
