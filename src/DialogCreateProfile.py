# encoding: utf-8
#

from gi.repository import Gtk, GLib


class DialogCreateProfile(Gtk.Dialog):
    def __init__(self, parent, model, view):
        Gtk.Dialog.__init__(self, transient_for=parent, flags=0)
        title = "Create a profile - " + GLib.get_application_name()
        self.set_title(title)
        self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                         Gtk.STOCK_OK, Gtk.ResponseType.OK)

        # self.set_default_size(150, 100)
        label = Gtk.Label(label="Profile name")
        self._view = view
        self._model = model
        self.entry = Gtk.Entry(text="")

        box = self.get_content_area()
        box.set_margin_top(8)
        box.set_margin_start(16)
        box.set_margin_end(16)
        box.set_margin_bottom(8)
        box.pack_start(label, False, False, 0)
        box.pack_start(self.entry, False, False, 16)
        self.show_all()

    def dialog_run(self):
        if self.run() == Gtk.ResponseType.OK:
            text = self.entry.get_text()
            if text == "":
                self.dialog_run()
            else:
                self._model.create_profile(text)
                value = self._model.get_profile()
                items = self._model.list_profile()
                self._view.set_items(items)
                self._view.set_value(value)

                self.destroy()
        else:
            self.destroy()
